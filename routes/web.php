<?php
// FrontEnd Controllers

use App\Http\Controllers\CartController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\Auth\CustomLoginController;
use App\Http\Controllers\ProfileController;

Route::get('/',[HomeController::class,'index']);
Route::get('/product/details/{slug}', [ShopController::class, 'product_details'])->name('product.details');
Route::get('/get-sizes/{productColor}', [ShopController::class, 'colorSize']);
Route::get('/get-price/{colorSize}', [ShopController::class, 'getPrice']);

Route::get('/about',[HomeController::class, 'about'])->name('about');
Route::get('/contact-us',[HomeController::class, 'contact_us'])->name('contact.us');
Route::get('/faq',[HomeController::class,'faq'])->name('faq');
Route::get('/privacy-policy',[HomeController::class, 'privacy_policy'])->name('privacy.policy');
Route::get('/terms-conditions',[HomeController::class, 'terms_condition'])->name('terms.condition');
Route::get('/shipping-policys',[HomeController::class, 'shipping_policy'])->name('shipping.policy');
Route::get('/product/search', [ShopController::class, 'search'])->name('product.search');

Route::post('/send_message',[ContactController::class,'store'])->name('send.message');

Route::get('/find/product/{product}',[CartController::class,'find_product'])->name('find.product');
Route::get('/cart/count',[CartController::class,'cart_count'])->name('cart.count');
Route::post('/cart/update',[CartController::class,'update'])->name('cart.update');
Route::get('/cart/products',[CartController::class,'cart_products'])->name('cart.products');
Route::get('/cart',[CartController::class, 'cart'])->name('cart');
// web.php or routes file
Route::post('/orders/cancel', [CartController::class,'orderCancel'])->name('orders.cancle.request');
Route::get('/remove-item/{cart}',[CartController::class, 'remove_item'])->name('remove.item');
Route::get('/remove-all',[CartController::class, 'remove_all'])->name('cart.remove.all');
Route::get('/remove/details',[CartController::class, 'order_details'])->name('order.details');

// Checkout
Route::get('/checkout', [CartController::class, 'checkout'])->name('checkout');
Route::post('/cart/checkout', [CartController::class, 'cart_checkout'])->middleware('auth')->name('cart.checkout');
Route::get('/order-success/{order}', [CartController::class, 'invoice'])->name('order-success');

// Custom Login
Route::post('/custom/login', [CustomLoginController::class, 'login'])->name('custom.login');

//Profile
Route::post('/profile',[ProfileController::class,'profile'])->middleware('auth')->name('profile');

Route::post('/profile/create',[ProfileController::class,'create'])->name('profile.create');
Route::post('/profile/update/{profile}',[ProfileController::class,'update'])->name('profile.update');

// For add to cart
Route::post('add/cart/{slug}',[CartController::class,'addToCart'])->name('addTo.cart');

Route::get('/offer/{offer}',[ShopController::class, 'offer'])->name('offer');
Route::get('/offer-product/{product}/{offer}',[ShopController::class, 'offer_product'])->name('offer.product');
Route::post('offer/cart/{slug}',[CartController::class, 'offerCart'])->name('offer.cart');

Route::get('/shop/{category?}/{sub_category?}',[ShopController::class, 'filter'])->name('filter');
