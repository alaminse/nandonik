<?php

use App\Http\Controllers\Backend\ColorController;
use App\Http\Controllers\Backend\SizeController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\SubCategoryController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\SocalMediaController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\Backend\SiteSettingController;
use App\Http\Controllers\Backend\OfferController;
use App\Http\Controllers\Backend\BarandController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/admin', function () {
    return redirect()->route('admin.login');
});

Route::get('/admin/login', function () {
    return view('backend.login');
})->name('admin.login');

Route::get('/dashboard', function () {
    if (Auth::check()) {
        $user = Auth::user();
        if ($user->hasAnyRole(['admin', 'manager'])) {
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->route('dashboard');
        }
    }
});

Route::get('/user/dashboard', [HomeController::class, 'dashboard'])->middleware('auth')->name('dashboard');
Route::get('/admin/dashboard', [AdminController::class, 'index'])->middleware('auth')->name('admin.dashboard');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function() {

    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('permissions', PermissionController::class);

    Route::controller(CategoryController::class)
        ->prefix('categories')
        ->as('categories.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{category}', 'show')->name('show');
            Route::get('/edit/{category}', 'edit')->name('edit');
            Route::post('/update/{category}', 'update')->name('update');
            Route::get('/destroy/{category}', 'destroy')->name('destroy');
            Route::get('/status/{category}', 'status')->name('status');
        });

    Route::controller(SubCategoryController::class)
        ->prefix('sub/categories')
        ->as('sub.categories.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{sub_category}', 'show')->name('show');
            Route::get('/edit/{sub_category}', 'edit')->name('edit');
            Route::post('/update/{sub_category}', 'update')->name('update');
            Route::get('/destroy/{sub_category}', 'destroy')->name('destroy');
            Route::get('/status/{sub_category}', 'status')->name('status');
        });
    Route::controller(BarandController::class)
        ->prefix('brands')
        ->as('brands.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{brand}', 'show')->name('show');
            Route::get('/edit/{brand}', 'edit')->name('edit');
            Route::post('/update/{brand}', 'update')->name('update');
            Route::get('/destroy/{brand}', 'destroy')->name('destroy');
            Route::get('/status/{brand}', 'status')->name('status');
        });
    Route::controller(OrderController::class)
        ->prefix('orders')
        ->as('orders.')
        ->group(function () {
            Route::get('/cancle/requests', 'cancle_request')->name('canclerequest');
            Route::get('{status}', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{order}', 'show')->name('show');
            Route::get('/edit/{category}', 'edit')->name('edit');
            Route::post('/update/{category}', 'update')->name('update');
            Route::get('/destroy/{category}', 'destroy')->name('destroy');
            Route::get('/status/{category}', 'status')->name('status');
            Route::post('/update-status/{order}','updateStatus')->name('updateStatus');
            Route::post('/cancle/statusUpdate/{cancle_order}','cancle_statusUpdate')->name('cancle.statusUpdate');
            Route::post('/payment/statusUpdate/{payment}','payment_statusUpdate')->name('payment.updateStatus');

        });

    Route::controller(ProductController::class)
        ->prefix('products')
        ->as('products.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{product}', 'show')->name('show');
            Route::get('/edit/{product}', 'edit')->name('edit');
            Route::post('/update/{product}', 'update')->name('update');
            Route::get('/destroy/{product}', 'destroy')->name('destroy');
            Route::get('/status/{product}', 'status')->name('status');
            Route::post('/meta/update/{product}', 'meta_update')->name('meta.update');
            Route::post('/image/update/{product}', 'image_update')->name('image.update');
            Route::get('/image/delete/{image}', 'image_delete')->name('image.delete');
            Route::get('/get/subcategories/{category}', 'getSubcategories');
            Route::get('/stock/out', 'stockOut')->name('stock.out');
            Route::get('/get/color_size/{color}/{size}', 'color_size');
            Route::post('/varient/update/{color_size}', 'varient_update')->name('varient.update');
        });

    Route::controller(SizeController::class)
            ->prefix('sizes')
            ->as('sizes.')
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::post('/store', 'store')->name('store');
                Route::post('/update/{size}', 'update')->name('update');
        });
    Route::controller(ColorController::class)
            ->prefix('colors')
            ->as('colors.')
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::post('/store', 'store')->name('store');
                Route::post('/update/{color}', 'update')->name('update');
        });

    Route::controller(SiteSettingController::class)
            ->prefix('pages')
            ->as('pages.')
            ->group(function () {
                Route::get('/home', 'home')->name('home');
                Route::get('/contact', 'contact')->name('contact');
                Route::get('/message_delete', 'delete')->name('message.delete');
                Route::get('/faq', 'faq')->name('faq');
                Route::get('/about', 'about')->name('about');
                Route::get('/privacy', 'privacy')->name('privacy');
                Route::get('/termsAndCondition', 'terms')->name('termsAndCondition');
                Route::get('/shippingAndReturn', 'shipping')->name('shipping');
                Route::post('/update/{service}', 'update')->name('update');
        });

    Route::POST('/delete_message/{delet_item}',[ContactController::class,'delete'])->name('delete.message');
    Route::POST('/update_socialmedia/{id}',[SocalMediaController::class,'update'])->name('socal.media.update');
    Route::get('socalmedia/address',[SocalMediaController::class,'index'])->name('socal.media');
    Route::post('logo/update/{logo_update}',[SocalMediaController::class,'logo_update'])->name('logo.update');

    Route::controller(OfferController::class)
        ->prefix('offers')
        ->as('offer.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/show/{offer}', 'show')->name('show');
            Route::get('/edit/{offer}', 'edit')->name('edit');
            Route::post('/update/{offer}', 'update')->name('update');
            Route::get('/destroy/{offer}', 'destroy')->name('destroy');
            Route::get('/status/{offer}', 'status')->name('status');
            Route::get('/send/sms/{offer}', 'send_sms')->name('send.sms');
            Route::post('/send/mail', 'send_mail')->name('send.mail');
        });
    Route::get('all/transactions',[AdminController::class,'allTransactions'])->name('all.transactions');
    Route::get('customer/lists',[AdminController::class,'customerLists'])->name('customer.lists');
    Route::get('cache-clear',[AdminController::class,'cacheClear'])->name('cache-clear');
});


