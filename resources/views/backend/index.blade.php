@extends('layouts.backend')
@section('title', 'Dashboard')

@section('css')
    <link href="{{ asset('backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
@endsection
@section('content')

    <div class="container mt-4">
        <div class="row">
            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-user"></i> Total User</h5>
                        <h2 class="card-text count display-4">{{ $data['users'] }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-user"></i> Total Product</h5>
                        <h2 class="card-text count display-4">{{ $data['product'] }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-clock-o"></i> Total Order</h5>
                        <h2 class="card-text count display-4">{{ $data['orderCount'] }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-user"></i> Total Product category</h5>
                        <h2 class="card-text count display-4 green">{{ $data['catagory'] }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-user"></i> Total Product brand</h5>
                        <h2 class="card-text count display-4">{{ $data['brand'] }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title"><i class="fa fa-user"></i> Available Stock Count</h5>
                        <h2 class="card-text count display-4">{{ $data['stock'] }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
              <div class="x_title">
                <h2 class="text-center">Latest Orders</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
                <div class="x_content">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            @include('backend.includes.message')
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Customer Name</th>
                                        <th>Shipping Area</th>
                                        <th>Shipping Charge</th>
                                        <th>Discount</th>
                                        <th>Coupon</th>
                                        <th>Products Price</th>
                                        <th>Total</th>
                                        <th>Payment Status</th>
                                        <th>Status</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $data['latest_order'] as $key => $order)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $order->user->name }}</td>
                                        <td>{{ $order->shipping_area }}</td>
                                        <td>{{ $order->shipping_charge }}</td>
                                        <td>{{ $order->discount }}</td>
                                        <td>{{ $order->coupon }}</td>
                                        <td>{{ $order->products_price }}</td>
                                        <td>{{ $order->total }}</td>
                                        <td>
                                            <button type="button" class="btn btn-{{\App\Enums\PaymentStatus::from($order->payment->payment_status)->message()}} btn-sm" data-toggle="modal" data-target="#paymentModal-{{$order->payment->id}}" data-current-status="{{ $order->payment->id }}">
                                                {{\App\Enums\PaymentStatus::from($order->payment->payment_status)->title()}}
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-{{\App\Enums\OrderStatus::from($order->status)->message()}} btn-sm" data-toggle="modal" data-target="#exampleModal-{{$order->id}}" data-current-status="{{ $order->status }}">
                                                {{\App\Enums\OrderStatus::from($order->status)->title()}}
                                            </button>
                                            @if ($order->cancle_req)
                                                <button type="button" class="btn btn-{{\App\Enums\CancleRequestStatus::from($order->cancle_req->status)->message()}} btn-sm" data-toggle="modal" data-target="#cancleModal-{{$order->cancle_req->id}}" data-current-status="{{ $order->cancle_req->status }}">
                                                    Order Cancle Request
                                                </button>
                                            @endif
                                        </td>
                                        <td><a class="btn btn-info btn-sm" href="{{ route('orders.show', $order->id) }}"><i class="fa fa-eye"></i></a></td>
                                    </tr>

                                    @include('backend.order.paymentModal')
                                    @include('backend.order.statusModal')
                                    @if ($order->cancle_req)
                                        @include('backend.order.cancleReqModal')
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2>Available Products</h2>
                <div class="clearfix"></div>
            </div>
                <div class="x_content">
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            @include('backend.includes.message')
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Image</th>
                                        <th>Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['available_products'] as $key => $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>
                                            <img class="img-fluid" src="{{ getImageUrl($product->image) }}" alt="{{$product->name}}" width="50px" width="70px" >
                                        </td>
                                        <td>{{ $product->stock }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2>Stock Out Products</h2>
                <div class="clearfix"></div>
            </div>
                <div class="x_content">
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            @include('backend.includes.message')
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Image</th>
                                        <th>Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['stock_out_products'] as $key => $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>
                                            <img class="img-fluid" src="{{ getImageUrl($product->image) }}" alt="{{$product->name}}" width="50px" width="70px" >
                                        </td>
                                        <td> <strong class="text-danger">{{ $product->stock }}</strong></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @include('backend.includes.message')
    </div>

    @push('scripts')
        <!-- Datatables -->
        <script src="{{ asset('backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    @endpush
@endsection
