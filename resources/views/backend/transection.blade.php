@extends('layouts.backend')
@section('title', 'Transections')
@section('css')
    <link href="{{ asset('backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Transections</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
        <div class="x_content">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box table-responsive">
                    @include('backend.includes.message')
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>order_id</th>
                                <th>payment_method</th>
                                <th>banking_name</th>
                                <th>banking_number</th>
                                <th>amount</th>
                                <th>transaction_id</th>
                                <th>payment_status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transections as $key => $transection)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $transection->order_id }}</td>
                                <td>{{ $transection->payment_method ?? '--' }}</td>
                                <td>{{ $transection->banking_name ?? '--' }}</td>
                                <td>{{ $transection->banking_number ?? '--' }}</td>
                                <td>{{ $transection->amount ?? '--' }}</td>
                                <td>{{ $transection->transaction_id ?? '--' }}</td>
                                <td>
                                    <a href="#" class="btn btn-{{\App\Enums\OrderStatus::from($transection->payment_status)->message()}} btn-sm">{{\App\Enums\OrderStatus::from($transection->payment_status)->title()}}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <!-- Datatables -->
    <script src="{{ asset('backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>

@endpush
@endsection
