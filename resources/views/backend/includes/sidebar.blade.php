<div class="col-md-3 left_col">
    @php
        use App\Enums\OrderStatus;
    @endphp
    <div class="left_col scroll-view">
      <div class="navbar nav_title" style="border: 0;">
        <a href="{{ url('/') }}" class="site_title"><span class="pl-4">Nandonik</span></a>
      </div>

      <div class="clearfix"></div>

      <!-- menu profile quick info -->
      <div class="profile clearfix">
        <div class="profile_pic">
          <img src="{{asset('backend/images/img.jpg')}}" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
          <span>Welcome,</span>
          <h2>{{ auth()->user()->name }}</h2>
        </div>
      </div>
      <!-- /menu profile quick info -->

      <br />

      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <h3>General</h3>
          <ul class="nav side-menu">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i>Dashboard</a></li>
                <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('roles.index') }}">Role Manage</a></li>
                        <li><a href="{{ route('users.index') }}">User Lists</a></li>
                        <li><a href="{{ route('users.create') }}">Add User</a></li>
                        <li><a href="{{ route('permissions.index') }}">Permission Manage</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-user"></i>Manage Categories <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('categories.index') }}">Category</a></li>
                        {{-- <li><a href="{{ route('categories.create') }}"> Add Category</a></li> --}}
                        <li><a href="{{ route('sub.categories.index') }}">Sub Category</a></li>
                        {{-- <li><a href="{{ route('sub.categories.create') }}"> Add Sub Category</a></li> --}}

                    </ul>
                </li>
                <li><a><i class="fa fa-user"></i>Manage Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        <li><a href="{{ route('colors.index') }}">Colors</a></li>
                        <li><a href="{{ route('sizes.index') }}">Sizes</a></li>
                        <li><a href="{{ route('products.index') }}">Products</a></li>
                        <li><a href="{{ route('products.create') }}">Add Product</a></li>
                        <li><a href="{{ route('products.stock.out') }}">Stock Out Products </a></li>
                    </ul>
                </li>
                <li><a href="{{ route('all.transactions')}}"><i class="fa fa-user"></i>Transactions</a></li>
                <li><a href="{{ route('customer.lists')}}"><i class="fa fa-user"></i>Customer Lists</a></li>
                <li><a><i class="fa fa-user"></i>Manage Offers<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        <li><a href="{{ route('offer.index') }}">Campaign Offer</a></li>
                        <li><a href="{{ route('offer.create') }}">Create new Offer</a></li>

                    </ul>
                </li>
                <li><a><i class="fa fa-user"></i>Brand<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        <li><a href="{{ route('brands.index') }}"> All Brand</a></li>
                        <li><a href="{{ route('brands.create') }}"> Add Brand</a></li>

                    </ul>
                </li>
                <li><a><i class="fa fa-user"></i>Manage Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('orders.index', 'all_order') }}">All Order</a></li>
                        <li><a href="{{ route('orders.index', 'confirm') }}">Confirm Orders</a></li>
                        <li><a href="{{ route('orders.index', 'pending') }}">Pending Orders</a></li>
                        <li><a href="{{ route('orders.index', 'ready_to_ship') }}">In Progress Orders</a></li>
                        <li><a href="{{ route('orders.index', 'delivered') }}">Delivered Orders</a></li>
                        <li><a href="{{ route('orders.index', 'out_for_delivery') }}">Out for Delivery</a></li>
                        <li><a href="{{ route('orders.index', 'cancle') }}">Canceled Orders</a></li>
                        <li><a href="{{ route('orders.canclerequest') }}">Cancele Request</a></li>

                    </ul>
                </li>
                <li><a><i class="fa fa-user"></i> Website Setting <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('pages.home') }}">Home</a></li>
                        <li><a href="{{ route('pages.about') }}">Our Story</a></li>
                        <li><a href="{{ route('pages.privacy') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('pages.faq') }}">FAQs</a></li>
                        <li><a href="{{ route('pages.termsAndCondition') }}">Terms &amp; Conditions</a></li>
                        <li><a href="{{ route('pages.shipping') }}">Shipping &amp; Return Policy</a></li>
                        <li><a href="{{route('cache-clear')}}">Cache Clear</a></li>
                        <li><a href="{{route('socal.media')}}"><i class="fa fa-user"></i>Site Information</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('pages.contact') }}"><i class="fa fa-user"></i>Queries</a></li>
                {{-- <li><a href="{{route('socal.media')}}"><i class="fa fa-user"></i>Manage Site</a></li> --}}
                {{-- <li><a><i class="fa fa-user"></i> System Backup <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('pages.home') }}">System Backup</a></li>
                        <li><a href="{{ route('pages.about') }}">Database Backup</a></li>
                    </ul>
                </li> --}}
          </ul>
        </div>
      </div>
      <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
      </div>
    </div>
</div>
