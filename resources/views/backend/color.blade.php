@extends('layouts.backend')
@section('title', 'Color Lists')
@section('css')
    <link href="{{ asset('backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<div class="col-md-8 col-sm-12">
    @include('backend.includes.message')
    <div class="x_panel">
      <div class="x_title">
        <h2>Color Lists</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
        <div class="x_content">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($colors as $key => $color)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $color->name }}</td>
                                <td> <span class="px-3 py-1" style="background-color: {{$color->code}}">{{ $color->code }}</span></td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal-{{$color->id}}">
                                        <i class="fa fa-edit"></i>
                                      </button>
                                </td>
                            </tr>

                            <div class="modal fade" id="exampleModal-{{$color->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('colors.update', $color->id) }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Color</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                    <div class="field form-group">
                                                        <label class="col-form-label col-12">Name<span class="required text-danger">*</span></label>
                                                        <div class="col-12">
                                                            <input class="form-control" name="name" placeholder="Name" value="{{$color->name}}" />
                                                        </div>
                                                    </div>
                                                    <div class="field form-group">
                                                        <label class="col-form-label col-12">Color Code<span class="required text-danger">*</span></label>
                                                        <div class="col-12">
                                                            <input class="form-control" name="code" placeholder="Color Code" value="{{$color->code}}" />
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-warning">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 col-sm-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Add New Color</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">

                    <form action="{{ route('colors.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="field form-group">
                            <label class="col-form-label col-12">Name<span class="required text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" name="name" placeholder="Name" value="{{old('name')}}" />
                            </div>
                        </div>
                        <div class="field form-group">
                            <label class="col-form-label col-12">Color Code<span class="required text-danger">*</span></label>
                            <div class="col-12">
                                <input class="form-control" name="code" placeholder="Color Code" value="{{old('code')}}" />
                            </div>
                        </div>
                        <div class="ln_solid">
                            <div class="form-group">
                                <div class="col-md-6 mt-3">
                                    <button type='submit' class="btn btn-primary">Create</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->


@push('scripts')
    <!-- Datatables -->
    <script src="{{ asset('backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>

@endpush
@endsection
