@extends('layouts.backend')

@section('title', 'Send SMS')
@section('css')
    <link href="{{ asset('backend/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="col-md-12 col-sm-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Send SMS </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li> <a class="btn btn-warning btn-small text-dark" href="{{ route('offer.index') }}"> Back</a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <span class="section">Offer</span>
            @include('backend.includes.message')
            <div class="row">
                <div class="col-12 mt-2">
                    <label class="form-label text-info">Title</label>
                    <input class="form-control" type="text" readonly value="{{ $offer->title }}">
                </div>
                <div class="col-sm-12 col-md-6 mt-3">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Offer Start</label>
                        <div class="col-md-6 col-sm-6">
                            <div class='input-group date' id='start'>
                                {!! $offer->offer_start !!}
                            </div>
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Offer End</label>
                        <div class="col-md-6 col-sm-6">
                            <div class='input-group date' id='end'>
                                {{ $offer->offer_end }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 mt-3">
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Image</label>
                        <div class="col-md-6 col-sm-6">
                            <img class="img-fluid" id="image_preview" src="{{ getImageUrl($offer->image) }}"
                                alt="Profile Image" accept="image/png, image/jpeg" width="400px" height="209px">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <label class="form-label text-info">Description</label>
                    <p class="text-justify">{!! $offer->description !!}</p>
                </div>
            </div>

            <h3 class="mt-2">Email Template</h3>
            <form action="{{ route('offer.send.mail') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <label for="subject">Subject</label>
                        <input type="text" id="subject" placeholder="Subject" class="form-control" name="subject">
                    </div>
                    <div class="col-12 mt-2">
                        <textarea class="form-control" placeholder="Description" name="description"rows="10"></textarea>
                    </div>
                    <div class="col-12 mt-3">
                        <button type='submit' class="btn btn-primary">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')
    <script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>
    <script src="{{ asset('backend/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        CKEDITOR.replace('description');
        $(function () {
            $('#start').datetimepicker();
        });

        $(function () {
            $('#end').datetimepicker();
        });

        CKEDITOR.replace( 'meta_description' );

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endpush

@endsection
