@extends('layouts.backend')
@section('title', $data['title'])
@section('css')
    <link href="{{ asset('backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>{{$data['title']}} <small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
        <div class="x_content">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box table-responsive">
                    @include('backend.includes.message')
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Customer Name</th>
                                <th>Shipping Area</th>
                                <th>Shipping Charge</th>
                                <th>Discount</th>
                                <th>Coupon</th>
                                <th>Products Price</th>
                                <th>Total</th>
                                <th>Payment Status</th>
                                <th>Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['orders'] as $key => $order)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $order->user->name }}</td>
                                <td>{{ $order->shipping_area }}</td>
                                <td>{{ $order->shipping_charge }}</td>
                                <td>{{ $order->discount }}</td>
                                <td>{{ $order->coupon }}</td>
                                <td>{{ $order->products_price }}</td>
                                <td>{{ $order->total }}</td>
                                <td>
                                    <button type="button" class="btn btn-{{\App\Enums\PaymentStatus::from($order->payment->payment_status)->message()}} btn-sm" data-toggle="modal" data-target="#paymentModal-{{$order->payment->id}}" data-current-status="{{ $order->payment->id }}">
                                        {{\App\Enums\PaymentStatus::from($order->payment->payment_status)->title()}}
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-{{\App\Enums\OrderStatus::from($order->status)->message()}} btn-sm" data-toggle="modal" data-target="#exampleModal-{{$order->id}}" data-current-status="{{ $order->status }}">
                                        {{\App\Enums\OrderStatus::from($order->status)->title()}}
                                    </button>
                                    @if ($order->cancle_req)
                                        <button type="button" class="btn btn-{{\App\Enums\CancleRequestStatus::from($order->cancle_req->status)->message()}} btn-sm" data-toggle="modal" data-target="#cancleModal-{{$order->cancle_req->id}}" data-current-status="{{ $order->cancle_req->status }}">
                                            Order Cancle Request
                                        </button>
                                    @endif
                                </td>
                                <td><a class="btn btn-info btn-sm" href="{{ route('orders.show', $order->id) }}"><i class="fa fa-eye"></i></a></td>
                            </tr>

                            @include('backend.order.paymentModal')
                            @include('backend.order.statusModal')
                            @if ($order->cancle_req)
                                @include('backend.order.cancleReqModal')
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->


@push('scripts')
    <!-- Datatables -->
    <script src="{{ asset('backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>

@endpush
@endsection
