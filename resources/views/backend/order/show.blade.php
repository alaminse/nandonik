@extends('layouts.backend')

@section('title', 'Show order')
@section('content')

<div class="col-md-12 col-sm-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Show order <small><a class="btn btn-warning" href="{{ route('orders.index', 'all_order') }}"> Back</a></small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <span class="section">Order Details</span>
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Order ID:</strong> {{ $order->order_id }}</p>
                    <p><strong>Shipping Area:</strong> {{ $order->shipping_area }}</p>
                    <p><strong>Shipping Charge:</strong> {{ $order->shipping_charge }}</p>
                    <p><strong>Discount:</strong> {{ $order->discount }}</p>
                    <p><strong>Coupon:</strong> {{ $order->coupon }}</p>
                    <p><strong>Products Price:</strong> {{ $order->products_price }}</p>
                    <p><strong>Total:</strong> {{ $order->total }}</p>
                    <p><strong>Status:</strong> <span class="badge badge-{{\App\Enums\OrderStatus::from($order->status)->message()}} btn-sm">{{\App\Enums\OrderStatus::from($order->status)->title()}}</span></p>
                </div>
                <div class="col-md-6">
                    <h5>Payment Details</h5>
                    <p><strong>Payment Method:</strong> {{ $order->payment->payment_method }}</p>
                    <p><strong>Banking Name:</strong> {{ $order->payment->banking_name ?: 'N/A' }}</p>
                    <p><strong>Banking Number:</strong> {{ $order->payment->banking_number ?: 'N/A' }}</p>
                    <p><strong>Amount:</strong> {{ $order->payment->amount ?: 'N/A' }}</p>
                    <p><strong>Transaction ID:</strong> {{ $order->payment->transaction_id ?: 'N/A' }}</p>
                    <p><strong>Payment Status:</strong> {{ $order->payment->payment_status ? 'Paid' : 'Unpaid' }}</p>
                    <p><strong>Payment Created At:</strong> {{ $order->payment->created_at }}</p>
                    <p><strong>Payment Updated At:</strong> {{ $order->payment->updated_at }}</p>
                </div>
            </div>
            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <h2>Billing Adress Details <small></small></h2>
                <tr>
                    <th>Address</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Apartment</th>
                    <th>City</th>
                    <th>Post Code</th>
                    <th>Country</th>
                    <th>Note</th>
                </tr>
                <tbody>
                    <tr>
                        <td>{{ $order->billing->address ?? $order->billing->billingAddress->address ?? 'N/A' }}</td>
                        <td>{{ $order->billing->first_name ?? $order->billing->billingAddress->first_name ?? 'N/A' }}</td>
                        <td>{{ $order->billing->last_name ?? $order->billing->billingAddress->last_name ?? 'N/A' }}</td>
                        <td>{{ $order->billing->email ?? $order->billing->billingAddress->email ?? 'N/A' }}</td>
                        <td>{{ $order->billing->phone ?? $order->billing->billingAddress->phone ?? 'N/A' }}</td>
                        <td>{{ $order->billing->appartment ?? $order->billing->billingAddress->appartment ?? 'N/A' }}</td>
                        <td>{{ $order->billing->city ?? $order->billing->billingAddress->city ?? 'N/A' }}</td>
                        <td>{{ $order->billing->post_code ?? $order->billing->billingAddress->post_code ?? 'N/A' }}</td>
                        <td>{{ $order->billing->country ?? $order->billing->billingAddress->country ?? 'N/A' }}</td>
                        <td>{{ $order->billing->note ?? $order->billing->billingAddress->note ?? 'N/A' }}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <h5>Product Details</h5>
                <tr>
                    <th>Name</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Sub Total</th>
                </tr>
                <tbody>
                    @foreach ($order->products as $key => $product)
                    <tr>
                        <td>{{ $product->product->name }}</td>
                        <td>{{ $product->color ?? 'N/A' }}</td>
                        <td>{{ $product->size ?? 'N/A' }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->discount }}</td>
                        <td>{{ $product->sub_total }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
