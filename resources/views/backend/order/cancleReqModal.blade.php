<div class="modal fade" id="cancleModal-{{$order->cancle_req->id}}" tabindex="-1" aria-labelledby="cancleModalLabel-{{$order->cancle_req->id}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('orders.cancle.statusUpdate', $order->cancle_req->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="cancleModalLabel-{{$order->cancle_req->id}}">Order Cancle Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12 mt-3">
                        <label>Cancle Reason<span class="required text-danger">*</span></label>
                        <br>
                        <p>{{ $order->cancle_req->note }}</p>
                    </div>
                    <div class="col-12 mt-3">
                        <label>Admin Note<span class="required text-danger">*</span></label>
                        <br>
                        <textarea class="form-control" name="admin_note" rows="4"></textarea>
                    </div>
                    <div class="col-12 mt-3">
                        <label>Order Status<span class="required text-danger">*</span></label>
                        <br>
                        <select name="status" class="form-control">
                            @foreach(\App\Enums\CancleRequestStatus::toArray() as $value => $label)
                                <option value="{{ $value }}" {{ $value == $order->cancle_req->status ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
