<div class="modal fade" id="paymentModal-{{$order->payment->id}}" tabindex="-1" aria-labelledby="paymentModal-{{$order->payment->id}}Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('orders.payment.updateStatus', $order->payment->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="paymentModal-{{$order->id}}Label">Payment Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <div class="col-12 mt-3">
                        <label>Order Status<span class="required text-danger">*</span></label>
                        <br>
                        <select name="payment_status" class="form-control" required value="{{ old('status') }}">
                            <option selected disabled>Choose Status</option>
                            @foreach(\App\Enums\PaymentStatus::toArray() as $value => $label)
                                <option value="{{ $value }}" {{ $value == $order->payment->payment_status ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
