<div class="modal fade" id="exampleModal-{{$order->id}}" tabindex="-1" aria-labelledby="exampleModal-{{$order->id}}Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('orders.updateStatus', $order->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModal-{{$order->id}}Label">Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <div class="col-12 mt-3">
                        <label>Order Status<span class="required text-danger">*</span></label>
                        <br>
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <select name="status" class="form-control" required value="{{ old('status') }}">
                            <option selected disabled>Choose Order Status</option>
                            @foreach(\App\Enums\OrderStatus::toArray() as $value => $label)
                                <option value="{{ $value }}" {{ $value == $order->status ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
