<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    @include('frontend.includes.meta')
    <title>
        @yield('front_title')
    </title>
</head>

<body class="template-index  home13-dark">

    @php
        $contact = contact();
    @endphp
    <!-- Page Loader -->
    @include('frontend.includes.pageLoader')
    <!-- End Page Loader -->
    <!-- Page Wrapper -->
    <div class="pageWrapper">
        <!-- Promotion Bar -->
        @include('frontend.includes.promossionbar')
        <!-- End Promotion Bar -->
        <!-- Search Form Drawer -->
        @include('frontend.includes.searchdrawer')
        <!-- End Search Form Drawer -->
        <!-- Main Header -->
        <div class="header-section clearfix animated">
            <!-- Desktop Header -->
            <div class="header-6 classic-style hdr-sticky ">
                <!-- Top Header  -->
                {{-- @include('frontend.includes.topheader') --}}
                <!-- Top header Ends -->
                <!-- Header -->
                @include('frontend.includes.header')
                <!-- Header Ends -->
                <!-- Nav bar Start -->
                @include('frontend.includes.navbar')
            </div>
            <!-- End Desktop Header -->
        </div>
        <!-- Nav Bar Ends -->
        <!-- Mobile Menu -->
        @include('frontend.includes.mobilenav')
        <!-- End Mobile Menu -->
        <!-- Body Content -->
        <div id="page-content">
            @yield('content')
            <!-- Footer Start -->
            @include('frontend.includes.footer')
            <!-- Footer Ends -->

            <!-- Scoll Top -->
            <div id="site-scroll"><i class="icon an an-angle-up"></i></div>
            <!-- End Scoll Top -->

            <!-- Minicart Drawer -->
            @include('frontend.includes.minicraft')
            <!-- End Minicart Drawer -->
            @include('frontend.includes.js')

        </div>

        <script>
            let cartCount = 0;

            function updateCartCount() {

                $.ajax({
                    type: 'GET',
                    url: '{{ route('cart.count') }}',
                    success: function(response) {
                        $('#CartCount').text(response.cartCount);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        console.log(xhr);
                    }
                });
            }

            $(document).ready(function() {
                updateCartCount();

                $('#icon-cart').on('click', function(e) {

                    e.preventDefault();
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('cart.products') }}',
                        success: function(response) {
                            products = response.data.products;
                            total = response.data.total;

                            const minicartList = document.querySelector('.mini-products-list');
                            minicartList.innerHTML = '';

                            products.forEach(product => {
                                const productHtml = `
                                    <li class="item">
                                        <a class="product-image" href="{{url('product')}}/${product.slug}"><img
                                            src="${product.image}"
                                            alt="${product.name}" title=""></a>
                                        <div class="product-details">
                                            <a href="{{url('cart')}}" class="edit-i remove" data-bs-toggle="tooltip" data-bs-placement="top"
                                                title="Edit"><i class="an an-edit" aria-hidden="true"></i></a>
                                            <a class="pName" href="{{url('product')}}/${product.slug}">${product.name}</a>
                                            <div class="variant-cart">${product.color} / ${product.size}</div>
                                            <div class="wrapQtyBtn clearfix">
                                                <span class="label">Qty:</span>
                                                <div class="qtyField clearfix">
                                                    <input readonly type="text" name="quantity" value="${product.quantity}" class="product-form__input qty">
                                                </div>
                                            </div>
                                            <div class="priceRow clearfix">
                                                <div class="product-price">
                                                    <div class="money"><strong>Price </strong>৳ ${product.price}</div>
                                                    <div class="money"><strong>Sub Total </strong>৳ ${product.sub_total}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                `;
                                minicartList.insertAdjacentHTML('beforeend', productHtml);
                            });

                            document.getElementById('total').textContent = '৳ ' + total.toFixed(2);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            console.log(xhr);
                        }
                    });
                    $('#minicart-drawer').modal('show');
                });
            });

            document.addEventListener('DOMContentLoaded', function () {
                const termsCheckbox = document.getElementById('termsCheckbox');
                const cartCheckoutButton = document.getElementById('cartCheckout');

                termsCheckbox.addEventListener('change', function() {
                    if (this.checked) {
                        cartCheckoutButton.classList.remove('disabled');

                        // Show a SweetAlert when the checkbox is checked
                        Swal.fire({
                            title: 'Success!',
                            text: 'You have agreed to the terms and conditions.',
                            icon: 'success',
                            confirmButtonText: 'Proceed'
                        });
                    } else {
                        cartCheckoutButton.classList.add('disabled');
                    }
                });
            });

            function confirmRemove(routeUrl) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, remove it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                    window.location.href = routeUrl; // Redirect to the specified URL
                    }
                });
            }
        </script>

        {!! Toastr::message() !!}
</body>

</html>
