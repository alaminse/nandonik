@extends('layouts.frontend')
@section('front_title', 'Home')
@section('content')
<!-- Home Banner Starts -->
@isset($data['section_1'])
<div class="slideshow slideshow-wrapper pb-section sliderFull">
    <div class="home-slideshow">
        <div class="slide slide1 d-block">
            <div class="slideimg blur-up lazyload bg-size">
                <img class="blur-up lazyload bg-img"
                    data-src="{{getImageUrl($data['section_1']->image)}}"
                    src="{{getImageUrl($data['section_1']->image)}}" alt="{{$data['section_1']->title ?? ''}}"
                    title="{{$data['section_1']->title ?? ''}}" />
                <div class="slideshow__text-wrap slideshow__overlay classic">
                    <div class="slideshow__text-content mt-0 text-shadow center">
                        <div class="container">
                            <div class="wrap-caption center bgbox">
                                <h2 class="h1 mega-title slideshow__title">{{$data['section_1']->title ?? ''}}</h2>
                                <a href="{{ route('offer', $data['section_1']->slug) }}" class="btn btn--large">{{$data['section_1']->button_name ?? ''}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset
<!-- Home banner Ends -->

@isset($data['brands'])
<div class="product-rows section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-header text-center">
                    <h2 class="h2">Brands</h2>
                </div>
                <!-- Product List -->
                <div class="grid-products grid-products-hover-btn">
                    <div class="productSlider">
                        @foreach ($data['brands'] as $brand)
                            @include('frontend.includes.brand')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset
<!-- Collection Tab slider -->
@isset($data['best_sell'])
<div class="tab-slider-product section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-header text-center">
                    <h2 class="h2">best selling products</h2>
                </div>
                <div class="tabs-listing">
                    <ul class="tabs clearfix">
                        <li rel="men" class="active">Men</li>
                        <li rel="women">Women</li>
                        <li rel="kids-babies">Kid</li>
                    </ul>
                    <div class="tab_container">
                        @foreach ($data['best_sell'] as $category)
                        <div id="{{ $category['slug'] }}" class="tab_content grid-products grid-products-hover-btn">
                            <div class="productSlider">
                                @foreach ($category['products'] as $product)
                                    @include('frontend.includes.product')
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset


@isset($data['categories'])
<div class="product-rows section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-header text-center">
                    <h2 class="h2">Categories</h2>
                </div>
                <!-- Product List -->
                <div class="grid-products grid-products-hover-btn">
                    <div class="productSlider">
                        @foreach ($data['categories'] as $category)
                            @include('frontend.includes.category')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset

<div class="section feature-content no-pt-section pt-0 my-5">
    @isset($data['section_2'])
    <div class="feature-row">
        <div class="col-12 col-sm-12 col-md-6 feature-row__item feature-row__text text-left">
            <div class="row-text">
                <h2 class="h2">{{$data['section_2']->title ?? ''}}</h2>
                <a href="{{ route('offer', $data['section_2']->slug) }}" class="btn border-btn-2 btn--large">{{$data['section_2']->button_name ?? ''}}</a>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 feature-row__item text-center">
            <img class="blur-up lazyload" data-src="{{getImageUrl($data['section_2']->image)}}"
                src="{{getImageUrl($data['section_2']->image)}}" alt="Just Landed"
                title="Just Landed" />
        </div>
    </div>
    @endisset
    @isset($data['section_3'])
    <div class="feature-row">
        <div class="col-12 col-sm-12 col-md-6 feature-row__item text-center">
            <img class="blur-up lazyload" data-src="{{getImageUrl($data['section_3']->image)}}"
                src="{{getImageUrl($data['section_3']->image)}}" alt="Leather Bags"
                title="Leather Bags" />
        </div>
        <div class="col-12 col-sm-12 col-md-6 feature-row__item feature-row__text text-left">
            <div class="row-text">
                <h2 class="h2">{{$data['section_3']->title ?? ''}}</h2>
                <a href="{{ route('offer', $data['section_3']->slug) }}" class="btn border-btn-2 btn--large">{{$data['section_3']->button_name ?? ''}}</a>
            </div>
        </div>
    </div>
    @endisset
</div>
@isset($data['latest_products'])
<div class="product-rows section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="section-header text-center">
                    <h2 class="h2">Fresh Arrivals</h2>
                </div>
                <!-- Product List -->
                <div class="grid-products grid-products-hover-btn">
                    <div class="productSlider">
                        @foreach ($data['latest_products'] as $product)
                            @include('frontend.includes.product')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endisset
</div>
<!-- End Body Content -->
@endsection
