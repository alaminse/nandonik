<div class="modal fade" id="orderCancle-{{$order->id}}" tabindex="-1"
    aria-labelledby="orderCancleLabel-{{$order->id}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <form action="{{ route('orders.cancle.request') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$order->id}}" name="order_id">
                <div class="modal-header">
                    <h5 class="modal-title" id="orderCancleLabel-{{$order->id}}">Order Cancle Request</h5>
                    <button type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="col-12 mt-3">
                        <label>Order Status<span class="required text-danger">*</span></label>
                        <h6 class="badge bg-{{ \App\Enums\OrderStatus::from($order->status)->message() }}">{{ \App\Enums\OrderStatus::from($order->status)->title() }}</h6>
                    </div>
                    <div class="col-12 mt-3">
                        <label>Note<span class="required text-danger">*</span></label>
                        <textarea name="note" rows="4" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>

            </form>
        </div>
    </div>
</div>
