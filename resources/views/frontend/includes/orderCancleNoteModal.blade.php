<div class="modal fade" id="orderCancleNote-{{$order->cancle_req->id ?? ''}}" tabindex="-1"
    aria-labelledby="orderCancleNoteLabel-{{$order->cancle_req->id ?? ''}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-2">
                <div class="modal-header">
                    <h5 class="modal-title" id="orderCancleNoteLabel-{{$order->cancle_req->id ?? ''}}">Order Cancle Note</h5>
                    <button type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    @if ($order->cancle_req)
                        <div class="col-12 mt-3">
                            <label>Order Status<span class="required text-danger">*</span></label>
                            <h6 class="badge bg-{{ \App\Enums\OrderStatus::from($order->status)->message() }}">{{ \App\Enums\OrderStatus::from($order->status)->title() }}</h6>
                        </div>
                        <div class="col-12 mt-3">
                            <label>Cancle Request Status<span class="required text-danger">*</span></label>
                            <h6 class="badge bg-{{ \App\Enums\CancleRequestStatus::from($order->cancle_req->status)->message() }}">{{ \App\Enums\CancleRequestStatus::from($order->cancle_req->status)->title() }}</h6>
                        </div>
                        <div class="col-12 mt-3">
                            <label>Note<span class="required text-danger">*</span></label>
                            <p>{{ $order->cancle_req->note ?? '' }}</p>
                        </div>
                        <div class="col-12 mt-3">
                            <label>Feedback<span class="required text-danger">*</span></label>
                            <p>{{ $order->cancle_req->admin_note ?? '' }}</p>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
