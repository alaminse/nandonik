<div class="col-12 item">
    <!-- Product Image -->
    <div class="product-image">
        <!-- product Image -->
        <a href="{{ route('filter', ['brand' => $brand->slug]) }}">
            <!-- Image -->
            <img class="primary blur-up lazyload" data-src="{{ getImageUrl($brand->image) }}" alt="{{$brand->name}}" title="{{$brand->name}}" />
            <!-- End {{$brand->name}} -->
            <!-- Hover Image -->
            <img class="hover blur-up lazyload" data-src="{{ getImageUrl($brand->image) }}" alt="{{$brand->name}}" title="{{$brand->name}}" />
            <!-- End Hover Image -->
        </a>
    </div>
</div>
