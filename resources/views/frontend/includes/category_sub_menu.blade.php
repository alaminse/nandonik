<ul class="row grid--uniform mmWrapper">
    @foreach (nav_menu()['categories_with_subcategories'] as $category)
        <li class="lvl-1 col-md-3 col-lg-3">
            <a href="{{ route('shop', ['slug' => 'category', 'option' =>  $category->slug ]) }}" class="site-nav lvl-1">{{ $category->name }}</a>
            <ul class="subLinks">
                @foreach ($category->sub_categories as $item)
                    <li class="lvl-2"><a href="{{ route('shop', ['slug' => 'sub_category', 'option' => $category->slug, 'option2' => $item->slug]) }}" class="site-nav"> {{$item->name}} </a></li>
                @endforeach
            </ul>
        </li>
    @endforeach
</ul>
