<!-- Desktop Navigation -->
<div class="main-navigation d-none d-lg-block">
    <div class="d-flex" id="AccessibleNav">
        <!-- Desktop Menu -->
        <nav class="grid__item p-2">
            <ul id="siteNav" class="d-flex flex-wrap site-nav medium left m-auto hidearrow">
                @if (nav_menu()['offers'])
                    <li class="lvl1 parent dropdown">
                        <a href="#">Offers <i class="an an-angle-down"></i></a>
                        <ul class="dropdown">
                            @foreach (nav_menu()['offers']  as $item)
                                <li><a href="{{ route('offer', $item->slug) }}" class="site-nav">{{ $item->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if (brands())
                    <li class="lvl1 parent megamenu">
                        <a href="#">Brands <i class="an an-angle-down"></i></a>
                        <ul class="dropdown">
                            @foreach (brands()->shuffle()->take(8) as $brand)
                                <li>
                                    <a href="{{ route('filter', ['brand' => $brand->slug]) }}" class="site-nav">{{ $brand->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if (nav_menu()['new_in'])
                    <li class="lvl1 parent megamenu">
                        <a href="#">New In <i class="an an-angle-down"></i></a>
                        <ul class="dropdown">
                            @foreach (nav_menu()['new_in']  as $item)
                                <li><a href="{{ route('product.details', $item->slug)}}" class="site-nav">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if (categories())
                    <li class="lvl1 parent megamenu">
                        <a href="#">Categories <i class="an an-angle-down"></i></a>
                        <div class="megamenu style1">
                            <div class="row">
                                <div class="lvl-1 col-12">
                                    <ul class="row grid--uniform mmWrapper">
                                        @foreach (categories() as $category)
                                            <li class="lvl-1 col-md-3 col-lg-3">
                                                <a href="{{ route('filter', [$category->slug]) }}" class="site-nav lvl-1">{{ $category->name }}</a>
                                                <ul class="subLinks">
                                                    @foreach ($category->sub_categories as $sub_category)
                                                        <li class="lvl-2"><a href="{{ route('filter', [$category->slug, $sub_category->slug]) }}" class="site-nav"> {{$sub_category->name}} </a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
                <li class="lvl1 parent megamenu">
                    <a href="{{ route('filter') }}">Shop</a>
                </li>
            </ul>
        </nav>
        <!-- Search Bar  -->
        <div class="site-header__search search-bar-inline ms-auto p-2">
            <form action="{{ route('filter') }}" class="search-bar__form d-flex position-relative">
                <input class="search__input" type="search" name="q" placeholder="Search entire store..."
                       aria-label="Search" autocomplete="off" />
                <button class="go-btn search__button" type="submit"><i class="icon an an-search"></i></button>
            </form>
        </div>
        <!-- End Desktop Menu -->
    </div>
</div>
<!-- End Desktop Navigation -->
