<div class="mobile-nav-wrapper dark-wrapper" role="navigation">
    <div class="closemobileMenu"><i class="icon an an-times-circle closemenu"></i> Close Menu</div>
    <ul id="MobileNav" class="mobile-nav">
        <li class="lvl1 parent megamenu">
            <a href="{{ route('filter', ['women']) }}">Women</i></a>
        </li>
        <li class="lvl1 parent megamenu">
            <a href="{{ route('filter', ['men']) }}">Men</i></a>
        </li>
        <li class="lvl1 parent megamenu">
            <a href="{{ route('filter', ['kids']) }}">Kids</i></a>
        </li>
        @if (nav_menu()['offers'])
            <li class="lvl1 parent megamenu">
                <a href="#">Offers <i class="an an-angle-down"></i></a>
                <ul class="dropdown">
                    @foreach (nav_menu()['offers'] as $item)
                        <li><a href="{{ route('offer', $item->slug) }}" class="site-nav">{{ $item->title }}</a></li>
                    @endforeach
                </ul>
            </li>
        @endif
        @if (brands())
            <li class="lvl1 parent megamenu">
                <a href="#">Brands <i class="an an-angle-down"></i></a>
                <ul class="dropdown">
                    @foreach (brands()->shuffle()->take(8) as $brand)
                        <li>
                            <a href="{{ route('filter', ['brand' => $brand->slug]) }}" class="site-nav">{{ $brand->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif
        @if (nav_menu()['new_in'])
            <li class="lvl1 parent megamenu">
                <a href="#">New In <i class="an an-angle-down"></i></a>
                <ul class="dropdown">
                    @foreach (nav_menu()['new_in'] as $item)
                        <li><a href="{{ route('product.details', $item->slug)}}" class="site-nav">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </li>
        @endif
        @if (categories())
            <li class="lvl1 parent megamenu">
                <a href="#">Categories<i class="an an-angle-down"></i></a>
                <ul>
                    @foreach (categories() as $category)
                        <li>
                            <a href="{{ route('filter', [$category->slug]) }}" class="site-nav">{{ $category->name }}  <i class="an an-plus"></i></a>
                            @if($category->sub_categories)
                            <ul>
                                @foreach ($category->sub_categories as $sub_category)
                                    <li><a href="{{ route('filter', [$category->slug, $sub_category->slug]) }}" class="site-nav">{{ $sub_category->name }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif
        <li class="lvl1 parent megamenu">
            <a href="{{ route('filter') }}">Shop</a>
        </li>
    </ul>
</div>
