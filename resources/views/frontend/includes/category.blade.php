<div class="col-12 item">
    <!-- Product Image -->
    <div class="product-image">
        <!-- product Image -->
        <a href="{{ route('filter', ['$category' => $category->slug]) }}">
            <!-- Image -->
            <img class="primary blur-up lazyload" data-src="{{ getImageUrl($category->meta_image) }}" alt="{{$category->name}}" title="{{$category->name}}" />
            <!-- End {{$category->name}} -->
            <!-- Hover Image -->
            <img class="hover blur-up lazyload" data-src="{{ getImageUrl($category->meta_image) }}" alt="{{$category->name}}" title="{{$category->name}}" />
            <!-- End Hover Image -->
        </a>
    </div>
</div>
