<div class="table-responsive mt-4">
    <table class="table">
        <thead class="alt-font">
            <tr>
                <th>Order</th>
                <th>Shipping Area</th>
                <th>Shipping Charge</th>
                <th>Discount</th>
                <th>product Price</th>
                <th>Total</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if ($data['orders'])
                @foreach ($data['orders'] as $key => $order)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $order->shipping_area }}</td>
                        <td>{{ $order->shipping_charge }}</td>
                        <td>{{ $order->discount }}</td>
                        <td>{{ $order->products_price }}</td>
                        <td>{{ $order->total }}</td>
                        <td>
                            <p class="badge bg-{{ \App\Enums\OrderStatus::from($order->status)->message() }}">
                                {{ \App\Enums\OrderStatus::from($order->status)->title() }}</p>
                        </td>
                        <td>
                            @if (optional($order->cancle_req)->exists())
                            <button type="button" class="btn btn-success btn-sm"
                                data-bs-toggle="modal" data-bs-target="#orderCancleNote-{{$order->cancle_req->id}}">
                                    Cancel Reque
                            </button>
                            @else
                                <button type="button" class="btn btn-success btn-sm"
                                data-bs-toggle="modal" data-bs-target="#orderCancle-{{$order->id}}" {{ $order->status == 1 || $order->status == 3 ? '' : 'disabled' }} >
                                    Cancel Order
                                </button>
                            @endif
                            <button type="button" class="btn btn-success btn-sm"
                                data-bs-toggle="modal" data-bs-target="#orderDetails-{{$order->id}}">
                                View
                            </button>
                        </td>
                    </tr>

                    @include('frontend.includes.orderCancleModal')
                    @include('frontend.includes.orderDetailsModal')
                    @include('frontend.includes.orderCancleNoteModal')
                @endforeach
            @endif
        </tbody>
    </table>
</div>
