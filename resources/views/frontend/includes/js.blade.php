<!-- Including Javascript -->
<!-- Plugins JS -->
<script src="{{url('frontend/assets/js/plugins.js')}}"></script>
<!-- Main JS -->
<script src="{{url('frontend/assets/js/main.js')}}"></script>
<!-- Photo Swipwe Gallary only used for productlayout -->
<script src="{{url('frontend/assets/js/vendor/photoswipe.min.js')}}"></script>
<script src="{{url('frontend/assets/js/vendor/photoswipe-ui-default.min.js')}}"></script>
<!-- Contact Form JS -->
<script src="{{url('frontend/assets/js/ajax_sendmail.js')}}"></script>
{{-- Toastr JS and Jquery --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js">
</script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    toastr.options = {
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
    };
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.18/dist/sweetalert2.all.min.js"></script>
@stack('scripts')
