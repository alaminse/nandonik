<div class="top-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-10 col-sm-8 col-md-7 col-lg-4">
                <p class="phone-no float-start"><i class="icon an an-envelope me-1"></i><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 d-none d-md-none d-lg-block">
                <div class="text-center">
                    <p class="top-header_middle-text transform-none text-nowrap"></p>
                </div>
            </div>
            <div class="col-2 col-sm-4 col-md-5 col-lg-4 text-end d-none d-sm-block d-md-block d-lg-block">
                <div class="header-social">
                    <ul class="justify-content-end list--inline social-icons">
                        <li class="me-2">
                            <div class="item site-user-menu d-none d-sm-inline-block">
                                @guest
                                    <a href="{{route('login')}}" class="text-capitalize text-nowrap"><span
                                        class="text align-middle ms-1 d-none d-md-inline-block">Login</span></a>
                                    <a href="{{route('register')}}" class="text-capitalize text-nowrap"><span
                                        class="text align-middle ms-1 d-none d-md-inline-block">Register</span></a>
                                @else
                                    <a class="text align-middle ms-1 d-none d-md-inline-block" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                    {{-- <a href="{{ Auth::user()->hasAnyRole(['admin', 'manager']) ? route('admin.dashboard') : route('dashboard')}}" class="text-capitalize text-nowrap"><span
                                        class="text align-middle ms-1 d-none d-md-inline-block">Dashboard</span></a> --}}
                                @endguest
                            </div>
                        </li>
                        <li>
                            <a class="social-icons__link" href="{{ $contact->facebook }}" target="_blank" data-bs-toggle="tooltip"data-bs-placement="bottom" title="Facebook"><i class="icon an an-facebook"></i>
                            <span class="icon__fallback-text">Facebook</span></a>
                        </li>
                        <li>
                            <a class="social-icons__link" href="{{ $contact->linkedin }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Linked In"><i class="icon an an-linkedin"></i>
                            <span class="icon__fallback-text">Linked In</span></a>
                        </li>
                        <li>
                            <a class="social-icons__link" href="{{ $contact->instagram }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Instagram"><i class="icon an an-instagram"></i>
                            <span class="icon__fallback-text">Instagram</span></a>
                        </li>
                        <li>
                            <a class="social-icons__link" href="{{ $contact->youtube }}" target="_blank" data-bs-toggle="tooltip"data-bs-placement="bottom" title="YouTube"><i class="icon icon an an-youtube"></i>
                            <span class="icon__fallback-text">YouTube</span></a>
                        </li>
                        <li>
                            <a class="social-icons__link" href="{{ $contact->whatsapp }}" target="_blank" data-bs-toggle="tooltip"data-bs-placement="bottom" title="WhatsApp"><i class="icon icon an an-whatsapp"></i>
                            <span class="icon__fallback-text">WhatsApp</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2 col-sm-4 col-md-5 col-lg-4 text-end d-block d-sm-none d-md-none d-lg-none">
                <!-- Mobile User Links -->
                <div class="user-menu-dropdown">
                    <span class="user-menu"><i class="an an-user-alt"></i></span>
                    <ul class="customer-links list-inline" style="display:none;">
                        @guest
                            <li class="item"><a href="{{route('login')}}">Login</a></li>
                            <li class="item"><a href="{{route('register')}}">Register</a></li>
                        @else
                            <li class="item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
