<div class="minicart-right-drawer right modal fade" id="minicart-drawer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="minicart-header">
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><i class="an an-times"
                        aria-hidden="true" data-bs-toggle="tooltip" data-bs-placement="left" title="Close"></i></button>
                <h4 class="modal-title" id="myModalLabel2">Shopping Cart <strong></strong> items</h4>
            </div>
            <div class="minicart-body">
                <div class="empty-cart">
                    <p>You have no items in your shopping cart.</p>
                </div>
                <div id="drawer-minicart" class="block block-cart">
                    <ul class="mini-products-list">
                        
                    </ul>
                </div>
            </div>
            <div class="minicart-footer minicart-action">
                <div class="total-in">
                    <p class="label"><b>Total:</b><span class="item product-price"><span id="total"
                                class="totals"></span></span></p>
                </div>
                <div class="buttonSet d-flex flex-row align-items-center text-center">
                    <a href="{{ route('cart') }}" class="btn btn-secondary w-50 me-3">View Cart</a>
                    <a href="{{url('checkout')}}" class="btn btn-secondary w-50">Checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>
