<div class="slideshow slideshow-wrapper pb-section sliderFull">
        <div class="home-slideshow">
            <div class="slide slide1 d-block">
                <div class="slideimg blur-up lazyload bg-size">
                    <img class="blur-up lazyload bg-img" data-src="{{url('frontend/assets/images/slideshow-banners/home13-banner1.jpg')}}" src="{{url('frontend/assets/images/slideshow-banners/home13-banner1.jpg')}}" alt="Happy Customers" title="Happy Customers" />
                    <div class="slideshow__text-wrap slideshow__overlay classic">
                        <div class="slideshow__text-content mt-0 text-shadow center">
                            <div class="container">
                                <div class="wrap-caption center bgbox">
                                    <h2 class="h1 mega-title slideshow__title">Happy Customers</h2>
                                    <span class="mega-subtitle slideshow__subtitle">Save up to 50% off this weekend only</span>
                                    <a href="#" class="btn btn--large">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>