<div class="col-12 item">
    <div class="product-image">
        <a href="{{ route('product.details', $product->slug)}}">
            <img class="primary blur-up lazyload" data-src="{{ getImageUrl($product->image) }}" alt="{{$product->name}}" title="{{$product->name}}" />
            <img class="hover blur-up lazyload" data-src="{{ getImageUrl($product->thumbnail) }}" alt="{{$product->name}}" title="{{$product->name}}" />
            <div class="product-labels rectangular"><span class="lbl on-sale"> - {{ $product['discount_type'] === 'percent' ? $product['discount'] .'%' : $product['discount'] .' ৳'}} </span> <span class="lbl pr-label1">new</span></div>
        </a>
        @if(strtotime($product->discount_end) > time())
            <div class="saleTime desktop" data-countdown="{{ discountDate($product->discount_end) }}"></div>
        @endif

        <div class="addtocart btnicontext style6">
            <a href="{{ route('product.details', $product->slug)}}"  class="btn btn-addto-cart">
                <i class="icon an an-shopping-bag"></i>Add to Cart
            </a>
        </div>
    </div>
    <div class="product-details text-center">
        <div class="product-name">
            <a href="{{ route('product.details', $product->slug)}}">{{$product->name}}</a>
        </div>
        <div class="grid-view-item__vendor transform-none"></div>
        <div class="product-price">
            @if ($product->price > 0)

            @unless ($product['discount'] == 0 || discountCalculation($product) <= 0)
                <span class="old-price">৳ {{ $product->price }}</span>
                <span class="price">৳ {{ discountCalculation($product) }}</span>
            @else
                <span class="price">৳ {{ $product->price }}</span>
            @endunless
            @endif
        </div>

        <ul class="swatches">
            @foreach ($product->images as $image)
                <li class="swatch medium rounded"><img src="{{ getImageUrl($image->image) }}" alt="{{$product->name}}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$product->name}}" /></li>
            @endforeach
        </ul>
    </div>
</div>
