<div class="modal fade" id="orderDetails-{{$order->id}}" tabindex="-1"
    aria-labelledby="orderDetailsLabel-{{$order->id}}" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content p-2">
            <div class="modal-header">
                <h5 class="modal-title" id="orderDetailsLabel-{{$order->id}}">Order Details</h5>
                <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <dl class="row">
                    <dt class="col-sm-4">Order ID</dt>
                    <dd class="col-sm-8">{{ $order->order_id }}</dd>

                    <dt class="col-sm-4">Shipping Area</dt>
                    <dd class="col-sm-8">{{ $order->shipping_area }}</dd>

                    <dt class="col-sm-4">Shipping Charge</dt>
                    <dd class="col-sm-8">{{ $order->shipping_charge }}</dd>

                    <dt class="col-sm-4">Discount</dt>
                    <dd class="col-sm-8">{{ $order->discount }}</dd>

                    <dt class="col-sm-4">Coupon</dt>
                    <dd class="col-sm-8">{{ $order->coupon }}</dd>

                    <dt class="col-sm-4">Products Price</dt>
                    <dd class="col-sm-8">{{ $order->products_price }}</dd>

                    <dt class="col-sm-4">Total</dt>
                    <dd class="col-sm-8">{{ $order->total }}</dd>

                    <dt class="col-sm-4">Status</dt>
                    <dd class="col-sm-8">
                        <span class="badge bg-{{ \App\Enums\OrderStatus::from($order->status)->message() }}">{{ \App\Enums\OrderStatus::from($order->status)->title() }}</span>
                    </dd>

                    <dt class="col-sm-4">Created At</dt>
                    <dd class="col-sm-8">{{ $order->created_at }}</dd>

                    <dt class="col-sm-4">Updated At</dt>
                    <dd class="col-sm-8">{{ $order->updated_at }}</dd>
                </dl>

                <div class="row">
                    <div class="col-sm-12 col-md-4 mt-2">
                        <div class="table-responsive">
                            <h4>Payment Method</h4>
                            <div class="custom-table">
                                <div class="table-row table-header">
                                    <div class="table-cell">Payment Method</div>
                                    @if($order->payment->payment_method != 'cash_on_delivery')
                                        <div class="table-cell">Banking Number</div>
                                        <div class="table-cell">Amount</div>
                                        <div class="table-cell">Transaction ID</div>
                                    @endif
                                    <div class="table-cell">Status</div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">{{ $order->payment->payment_method }}</div>
                                    @if($order->payment->payment_method != 'cash_on_delivery')
                                        <div class="table-cell">{{ $order->payment->banking_number }}</div>
                                        <div class="table-cell">{{ $order->payment->amount }}</div>
                                        <div class="table-cell">{{ $order->payment->transaction_id }}</div>
                                    @endif
                                    <div class="table-cell">
                                        <p class="badge bg-{{ \App\Enums\PaymentStatus::from($order->payment->payment_status)->message() }}">
                                            {{ \App\Enums\PaymentStatus::from($order->payment->payment_status)->title() }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8 mt-2">
                        <div class="table-responsive">
                            <h4>Billing Address</h4>
                            <div class="custom-table">
                                <div class="table-row table-header">
                                    <div class="table-cell">First Name</div>
                                    <div class="table-cell">Last Name</div>
                                    <div class="table-cell">Email</div>
                                    <div class="table-cell">Phone</div>
                                    <div class="table-cell">Address</div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">{{ $order->billing->first_name ?? $order->billing->billingAddress->first_name }}</div>
                                    <div class="table-cell">{{ $order->billing->last_name ?? $order->billing->billingAddress->last_name }}</div>
                                    <div class="table-cell">{{ $order->billing->email ?? $order->billing->billingAddress->email }}</div>
                                    <div class="table-cell">{{ $order->billing->phone ?? $order->billing->billingAddress->phone }}</div>
                                    <div class="table-cell">{{ $order->billing->address ?? $order->billing->billingAddress->address }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive mt-2">
                    <h4>Products</h4>
                    <div class="custom-table">
                        <div class="table-row table-header">
                            <div class="table-cell">Name</div>
                            <div class="table-cell">Color</div>
                            <div class="table-cell">Size</div>
                            <div class="table-cell">Quantity</div>
                            <div class="table-cell">Price</div>
                            <div class="table-cell">Discount</div>
                            <div class="table-cell">Total</div>
                        </div>
                        @foreach ($order->products as $product)
                            <div class="table-row">
                                <div class="table-cell">{{ $product->product->name }}</div>
                                <div class="table-cell">{{ $product->color }}</div>
                                <div class="table-cell">{{ $product->size }}</div>
                                <div class="table-cell">{{ $product->quantity }}</div>
                                <div class="table-cell">{{ $product->price }}</div>
                                <div class="table-cell">{{ $product->discount }}</div>
                                <div class="table-cell">{{ $product->sub_total }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    .custom-table {
        display: table;
        width: 100%;
        border-collapse: collapse;
        margin-top: 10px; /* Adjust as needed */
    }

    .table-row {
        display: table-row;
    }

    .table-cell {
        display: table-cell;
        border: 1px solid #ddd;
        padding: 8px;
    }

    .table-header {
        font-weight: bold;
    }
</style>
