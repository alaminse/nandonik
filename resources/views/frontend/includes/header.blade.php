  <!-- Header -->
  <div class="header-wrap d-flex border-bottom">
      <div class="container-fluid">
          <div class="row align-items-center">
              <div class="col-4 col-sm-4 col-md-5 col-lg-8 d-block d-lg-none">
                  <button type="button" class="btn--link site-header__menu js-mobile-nav-toggle mobile-nav--open"
                      data-bs-toggle="tooltip" data-bs-placement="bottom" title="Menu"><i
                          class="icon an an-times"></i><i class="icon an an-bars"></i></button>
                  <!-- Mobile Search -->
                  <div class="site-header__search d-block d-lg-none mobile-search-icon">
                      <button type="button" class="search-trigger" data-bs-toggle="tooltip" data-bs-placement="bottom"
                          title="Search"><i class="icon an an-search"></i></button>
                  </div>
                  <!-- End Mobile Search -->
              </div>

              <div class="col-4 col-sm-4 col-md-4 col-lg-5 d-none d-lg-block">

                  <a class="common-btn" href="{{ route('filter', ['women'])}}">Women</a>
                  <a class="common-btn" href="{{ route('filter', ['men']) }}">Men</a>
                  <a class="common-btn" href="{{ route('filter', ['kids-babies']) }}">Kids</a>
              </div>

              <!-- Desktop Logo -->
              <div class="logo col-4 col-sm-4 col-md-2 col-lg-2 text-center">
                  <a href="{{ url('/') }}"><img src="{{ asset('uploads/logo/logo.jpg')}}" /></a>
              </div>
              <!-- End Desktop Logo -->

              <!-- Right Side -->
              <div class="col-4 col-sm-4 col-md-5 col-lg-5">
                  <div class="right-action text-action d-flex-align-center justify-content-end">
                      <!-- Search -->
                      <div class="item site-header__search d-none">
                          <button type="button" class="search-trigger"><i class="icon an an-search"></i><span
                                  class="text align-middle ms-1 d-none d-md-inline-block">Search</span></button>
                      </div>
                      <!-- End Search -->
                      <!-- User Links -->
                      <div class="item site-user-menu d-none d-sm-inline-block">
                        @guest
                            <a href="{{route('login')}}" class="icon-login text-capitalize text-nowrap"><i
                                class="icon an an-user-alt"></i><span
                                class="text align-middle ms-1 d-none d-md-inline-block">Login</span></a>
                            <a href="{{route('register')}}" class="icon-login text-capitalize text-nowrap"><span
                                class="text align-middle ms-1 d-none d-md-inline-block">Registration</span></a>
                        @else
                            <a href="{{ Auth::user()->hasAnyRole(['admin', 'manager']) ? route('admin.dashboard') : route('dashboard')}}" class="icon-login text-capitalize text-nowrap"><i
                                class="icon an an-user-alt"></i><span
                                class="text align-middle ms-1 d-none d-md-inline-block">{{ Auth::user()->name }}</span></a>
                        @endguest

                      </div>
                      <!-- End User Links -->
                      <!-- Minicart -->
                      <div class="item site-cart">
                          <a href="#" id="icon-cart" class="icon-cart site-header-cart btn-minicart text-capitalize">
                            <i class="icon an an-shopping-bag"></i>
                            <span class="text align-middle ms-1 d-none d-md-inline-block">Cart</span>
                            <span class="site-header__cart-count1 ms-1" data-cart-render="item_count">(<strong id="CartCount">0</strong>)</span>
                           </a>
                      </div>
                      <!-- End Minicart -->
                  </div>
              </div>
              <!-- End Right Side -->
          </div>
      </div>
  </div>
  <!-- End Header -->
