<!-- Footer -->
<footer id="footer">
    <div class="site-footer">
        <div class="footer-top">
            <div class="container">
                <!-- Footer Links -->
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                        <h4 class="h4">Customer Service</h4>
                        <ul>
                            <li><a href="{{route('contact.us')}}">Contact Us</a></li>
                            <li><a href="{{route('faq')}}">FAQs</a></li>
                            <li><a href="{{route('terms.condition')}}">Terms &amp; Conditions</a></li>
                            <li><a href="{{route('shipping.policy')}}">Shipping &amp; Returns Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                        <h4 class="h4">About Nandonik</h4>
                        <ul>
                            <li><a href="{{route('about')}}">Our Story</a></li>
                            <li><a href="{{route('privacy.policy')}}">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 contact-box">
                        <h4 class="h4">Contact Us</h4>
                        <ul class="addressFooter">
                            <li>
                                <i class="icon an an-map-marker"></i>
                                <p>{{ $contact->address }}</p>
                            </li>
                            <li class="phone">
                                <i class="icon an an-phone-volume"></i>
                                <p><a href="tel:"></a>{{ $contact->phone }}</p>
                            </li>
                            <li class="email">
                                <i class="icon an an-envelope"></i>
                                <p><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 newsletter">
                        <h3 class="h4">Connect with Us</h3>
                        <ul class="list--inline site-footer__social-icons social-icons mt-lg-0 mt-md-0 mt-3">
                            <li>
                                <a class="social-icons__link d-inline-block" href="{{ Str::startsWith($contact->facebook, ['http://', 'https://']) ? $contact->facebook : 'https://' . $contact->facebook }}" target="_blank">
                                    <i class="icon an an-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="social-icons__link d-inline-block" href="{{ Str::startsWith($contact->facebook, ['http://', 'https://']) ? $contact->facebook : 'https://' . $contact->linkedin }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="LinkedIn"><i class="icon an an-linkedin"></i></a>
                            </li>
                            <li>
                                <a class="social-icons__link d-inline-block" href="{{ Str::startsWith($contact->facebook, ['http://', 'https://']) ? $contact->facebook : 'https://' . $contact->instagram }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Instagram"><i class="icon an an-instagram"></i></a>
                            </li>
                            <li>
                                <a class="social-icons__link d-inline-block" href="{{ Str::startsWith($contact->facebook, ['http://', 'https://']) ? $contact->facebook : 'https://' . $contact->youtube }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="YouTube"><i class="icon an an-youtube"></i></a>
                            </li>
                            <li>
                                <a class="social-icons__link d-inline-block" href="{{ Str::startsWith($contact->facebook, ['http://', 'https://']) ? $contact->facebook : 'https://' . $contact->whatsapp }}" target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" title="WhatsApp"><i class="icon an an-whatsapp"></i>></a>
                            </li>
                        </ul>
                        <div class="payment_op">
                            <img src="{{ asset('we-accept-payment.png') }}">
                        </div>
                        <div class="store mt-2">
                            <a href="https://drive.google.com/file/d/1dcVftZJ3vXGUg66BFY5soV-c-w8q9PYH/view?usp=sharing" target="_blank">
                                <img src="{{ asset('play.png') }}" alt="Play Store">
                            </a>
                            <a href="#" target="_blank">
                                <img src="{{ asset('apple-store.jpg') }}" alt="Apple Store">
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Footer Links -->
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <!-- Footer Copyright -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 copyright text-center"><span>&copy; 2023 .</span> All Rights Reserved.</div>
                    <!-- End Footer Copyright -->
                </div>
            </div>
        </div>
    </div>
    
    <style>
    .store img {
    float: left;
    width: 35px; /* Set your desired width */
    height: 35px; /* Set your desired height */
    margin-right: 10px; /* Adjust margin as needed */
}
</style>
</footer>
<!-- End Footer -->




