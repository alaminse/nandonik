@extends('layouts.frontend')
@section('front_title', 'User Dashboard')
@section('content')
    @php
        $profile = auth()->user()->profile ?? null;
    @endphp
    <!-- Body Content -->
    <div id="page-content">
        <!-- Page Title -->
        <div class="login page section-header text-center mb-0">
            <div class="page-title">
                <div class="wrapper">
                    <h1 class="page-width">My Account</h1>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

        <div class="container">
            @include('backend.includes.message')
            <div class="row mb-4 mb-lg-5 pb-lg-5">
                <div class="col-xl-2 col-lg-2 col-md-12 md-margin-20px-bottom">
                    <!-- Nav tabs -->
                    <ul class="nav flex-column dashboard-list mb-4 mb-lg-0" role="tablist">
                        <li><a class="nav-link active text-white" data-bs-toggle="tab" href="#dashboard">Dashboard</a></li>
                        <li><a class="nav-link  text-white" data-bs-toggle="tab" href="#orders">Orders</a></li>
                        <li><a class="nav-link text-white" data-bs-toggle="tab" href="#address">Addresses</a></li>
                        <li><a class="nav-link text-white" data-bs-toggle="tab" href="#account-details">Account details</a>
                        </li>

                        <li>
                            <a class="nav-link text-white" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    <!-- End Nav tabs -->
                </div>

                <div class="col-xs-10 col-lg-10 col-md-12">
                    <!-- Tab panes -->
                    <div class="tab-content dashboard-content padding-30px-all md-padding-15px-all" style="">
                        <!-- Dashboard -->
                        <div id="dashboard" class="tab-pane fade active show">
                            <h3>Dashboard </h3>
                            <div class="row user-profile mt-4">
                                <div class="col-12 col-lg-6 text-dark">
                                    <div class="profile-img">
                                        <div class="img"><img src="{{ asset('frontend/assets/images/profile.jpg') }}"
                                                alt="profile" width="65" /></div>
                                        <div class="detail ms-3 ">
                                            <h5 class="mb-1 text-dark">{{ auth()->user()->name }}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <ul class="profile-order mt-3 mt-lg-0 text-dark">
                                        <li>
                                            <h3 class="mb-1 text-dark">{{ count($data['orders']) }}</h3>
                                            All Orders
                                        </li>
                                        <li>
                                            <h3 class="mb-1 text-dark">{{ $data['orders']->where('status', 3)->count() }}</h3>
                                            Confirm Orders
                                        </li>
                                        <li>
                                            <h3 class="mb-1 text-dark">{{ $data['orders']->where('status', 2)->count() }}</h3>
                                            Canceled Orders
                                        </li>
                                        <li>
                                            <h3 class="mb-1 text-dark">{{ $data['orders']->where('status', 6)->count() }}</h3>
                                            Delivered Orders
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @include('frontend.includes.order_table')
                        </div>
                        <!-- End Dashboard -->
                        <!-- Orders -->
                        <div id="orders" class="product-order tab-pane fade">
                            <h3>Orders</h3>

                            @include('frontend.includes.order_table')
                        </div>
                        <!-- End Orders -->
                        <!-- Address -->
                        <div id="address" class="address tab-pane">
                            <h3>Addresses</h3>
                            <p class="xs-fon-13 margin-10px-bottom">The following addresses will be used on the checkout
                                page by default.</p>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 table-responsive">
                                    <table class="table">
                                        <thead class="alt-font">
                                            <tr>
                                                <th>Address</th>
                                                <th>Country</th>
                                                <th>Postal Code</th>
                                                <th>City</th>
                                                <th>State</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $profile->address ?? '' }}</td>
                                                <td>{{ $profile->country ?? '' }}</td>
                                                <td>{{ $profile->postalcode ?? '' }}</td>
                                                <td>{{ $profile->city ?? '' }}</td>
                                                <td>{{ $profile->state ?? '' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @foreach (Auth::user()->billing_addresses as $key => $address)
                                    <div class="col-12 col-sm-6">
                                        <h4 class="billing-address">Billing address {{++$key}}</h4>
                                        <p>{{ $address->address }} <br> {{ $address->city }} <br> {{ $address->state }}
                                        </p>{{ $address->note }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- End Address -->
                        <!-- Account Details -->
                        <div id="account-details" class="tab-pane fade">
                            <h3>Account details </h3>
                            <div class="account-login-form bg-light-gray padding-20px-all">

                                @if ($profile != null)
                                    <form action="{{ route('profile.update', $profile->id) }}" method="POST">
                                        @csrf
                                    @else
                                        <form action="{{ route('profile.create') }}" method="POST">
                                            @csrf
                                @endif
                                <fieldset>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-firstname">First Name <span
                                                    class="required-f">*</span></label>
                                            <input name="fname" value="{{ auth()->user()->name }}" readonly
                                                id="input-firstname" class="form-control text-dark" type="text">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-lastname">Last Name <span
                                                    class="required-f">*</span></label>
                                            <input name="lname" value="{{ $profile->lname ?? '' }}"id="input-lastname"
                                                class="form-control" type="text">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-email">E-Mail <span class="required-f">*</span></label>
                                            <input name="email" readonly value="{{ auth()->user()->email }}"
                                                id="input-email" class="form-control text-dark" type="email">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-telephone">Telephone <span
                                                    class="required-f">*</span></label>
                                            <input name="phone" value="{{ $profile->phone ?? '' }}"
                                                id="input-telephone" class="form-control" type="tel">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-address">Address <span class="required-f">*</span></label>
                                            <input name="address" value="{{ $profile->address ?? '' }}"
                                                class="form-control" type="text">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-country">Country <span class="required-f">*</span></label>
                                            <input name="country" value="{{ $profile->country ?? '' }}"
                                                class="form-control" type="text">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-image">Postal Code</label>
                                            <input name="postalcode" value="{{ $profile->postalcode ?? '' }}"
                                                class="form-control" type="number">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-city">City</label>
                                            <input name="city" value="{{ $profile->city ?? '' }}"
                                                class="form-control" type="text">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-state">State</label>
                                            <input name="state" value="{{ $profile->state ?? '' }}"
                                                class="form-control" type="text">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                            <label for="input-image">Image <span class="required-f">*</span></label>
                                            <input name="image" value="{{ $profile->image ?? '' }}"
                                                class="form-control" type="file">
                                        </div>
                                    </div>

                                </fieldset>
                                <button type="submit" class="btn margin-15px-top btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- End Account Details -->
                    </div>
                    <!-- End Tab panes -->
                </div>
            </div>
        </div>
        <!-- End Body Container -->
    </div>
    <!-- End Body Content -->
@endsection
