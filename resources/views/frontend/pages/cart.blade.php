@extends('layouts.frontend')
@section('front_title', 'Cart')
@section('content')
<div id="page-content">
    <!-- Page Title -->
    <div class="page section-header text-center">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-title">Cart Page</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->

    <div class="container">
        <div class="row">
            <!-- Main Content -->
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">

                <form action="{{ route('cart.update')}}" method="post" class="cart style2">
                    @csrf

                    <table>
                        <thead class="cart__row cart__header">
                            <tr>
                                <th colspan="2" class="text-center">Product</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Discount</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Total</th>
                                <th class="action">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $discount = 0;
                            ?>
                            @foreach ($data['products'] as $product)
                            <?php
                                $discount += $product['discount'] * $product['quantity'];
                            ?>


                                <input type="hidden" name="cart_id[]" value="{{ $product['cart_id'] }}">
                                <tr class="cart__row border-bottom line1 cart-flex border-top">
                                    <td class="cart__image-wrapper cart-flex-item">
                                        <a href="{{ route('product.details', $product['slug'])}}"><img class="cart__image blur-up lazyload"
                                                data-src="{{ $product['image'] }}"
                                                src="{{ $product['image'] }}"
                                                alt="Elastic Waist Dress - Navy / Small" /></a>
                                    </td>
                                    <td class="cart__meta small--text-left cart-flex-item">
                                        <div class="list-view-item__title">
                                            <a href="{{ route('product.details', $product['slug'])}}">{{$product['name']}} </a>
                                        </div>
                                        <div class="cart__meta-text">
                                            Color: {{ $product['color'] }} <br>Size: {{ $product['size'] }}<br>
                                        </div>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item text-center">
                                        <span class="money">৳ {{ $product['price'] }}</span>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item text-center">
                                        <span class="money">৳ {{$product['discount']}}</span>
                                    </td>
                                    <td class="cart__update-wrapper cart-flex-item text-center">
                                        <div class="cart__qty text-center">
                                            <div class="qtyField">
                                                <a class="qtyBtn minus" href="javascript:void(0);"><i
                                                        class="icon an an-minus"></i></a>
                                                <input class="cart__qty-input qty" type="text" name="quantity[]" value="{{ $product['quantity'] }}"
                                                    pattern="[0-9]*" />
                                                <a class="qtyBtn plus" href="javascript:void(0);"><i
                                                        class="icon an an-plus"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="small--hide cart-price text-center">
                                        <span class="money">৳ {{ $product['sub_total'] }}</span>
                                    </td>
                                    <td class="text-center small--hide">
                                        <a href="#" class="btn btn--secondary cart__remove"
                                            onclick="confirmRemove('{{ route('remove.item', $product['cart_id']) }}'); return false;">
                                            <i class="icon an an-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3" class="text-start"><a href="{{ route('filter') }}"
                                        class="btn btn--link btn--small cart-continue"><i
                                            class="icon an an-chevron-circle-left"></i> Continue shopping</a>
                                </td>
                                <td colspan="3" class="text-end">
                                    <a href="#" class="btn btn--link btn--small small--hide"
                                        onclick="confirmRemove('{{ route('cart.remove.all') }}'); return false;">
                                        <i class="icon an an-times"></i> Clear Shoping Cart
                                    </a>
                                    <button type="submit"
                                        class="btn btn--link btn--small cart-continue ml-2"><i
                                            class="icon an an-sync"></i> Update Cart</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </form>
            </div>

            <div class="container mt-4">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 cart__footer">
                        <div class="solid-border">
                            <div class="row border-bottom pb-2 pt-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title"><strong>Discount</strong></span>
                                <span class="col-12 col-sm-6 cart__subtotal-title cart__subtotal text-right"><span
                                        class="money">৳ {{ $discount }}</span></span>
                            </div>
                            <div class="row border-bottom pb-2 pt-2">
                                <span class="col-12 col-sm-6 cart__subtotal-title"><strong>Subtotal</strong></span>
                                <span class="col-12 col-sm-6 cart__subtotal-title cart__subtotal text-right"><span
                                        class="money">৳ {{ $data['total'] }}</span></span>
                            </div>
                            <div class="cart__shipping"></div>
                            <div class="customCheckbox cart_tearm">
                                <input type="checkbox" value="allen-vela" id="termsCheckbox">
                                <label for="termsCheckbox"><a href="{{ route('terms.condition') }}" target="_blank">I agree with the terms and conditions</a></label>
                            </div>
                            <a href="{{route('checkout')}}" id="cartCheckout" class="btn btn--small-wide checkout disabled">Proceed To Checkout</a>
                            <div class="paymnet-img">
                                <img src="{{asset('frontend/assets/images/payment-img.jpg')}}" alt="Payment" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Main Content -->
        </div>
    </div>
</div>
@endsection
