@extends('layouts.frontend')
@section('front_title', 'Checkout')
@section('content')
<div id="page-content">
    <!-- Page Title -->
    <div class="page section-header text-center">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-title">Checkout Page</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->

    <style>

    </style>
    <div class="container">
        <div class="row">

            @include('backend.includes.message')
            <!-- Main Content -->
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                <div class="customer-box returning-customer">
                    <h3><i class="icon an an-user"></i> Returning customer?
                        @guest
                            <a href="#customer-login" id="customer" class="text-white" data-bs-toggle="collapse">Click here to login</a>
                        @endguest
                    </h3>
                    <div id="customer-login" class="collapse customer-content">
                        <div class="customer-info dropdown-body-color">
                            <p class="coupon-text mb-3">If you have shopped with us before, please enter your
                                details in the boxes below. If you are a new customer, please proceed to the
                                Billing &amp; Shipping section.</p>
                            <form method="post" action="{{ route('custom.login') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                        <label for="exampleInputEmail1">Email address <span
                                                class="required-f text-dark">*</span></label>

                                        <input id="exampleInputEmail1" type="email" class="no-margin @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                        <label for="exampleInputPassword1">Password <span
                                                class="required-f text-dark">*</span></label>

                                        <input id="exampleInputPassword1" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" >

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-check d-flex justify-content-between ps-0">
                                            <div class="customCheckbox">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                            @if (Route::has('password.request'))
                                                <a class="ml-3 float-end" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-primary mt-3">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                <div class="customer-box customer-coupon">
                    <h3 class="font-15 xs-font-13"><i class="icon an an-gift"></i> Have a coupon? <a href="#have-coupon"
                            class="text-white" data-bs-toggle="collapse">Click here to enter
                            your code</a></h3>
                    <div id="have-coupon" class="collapse coupon-checkout-content">
                        <div class="discount-coupon">
                            <div id="coupon" class="coupon-dec tab-pane active">
                                <p class="mb-3 text-dark">Enter your coupon code if you have one.</p>
                                <form method="post" action="#">
                                    <div class="form-group">
                                        <label class="required get text-dark" for="coupon-code"><span class="required-f text-dark">*</span>
                                            Coupon</label>
                                        <input id="coupon-code"  required type="text" class="mb-3 text-dark">
                                        <button type="submit" class="coupon-btn btn {{ Auth::check() ? '' : 'disabled'}}">{{ Auth::check() ? 'Apply Coupon' : 'Login First'}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="{{ route('cart.checkout') }}">
            @csrf
            <div class="row billing-fields">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3 mb-md-0">
                    <div class="row">
                        @if(Auth::user())
                        @foreach(Auth::user()->billing_addresses as $key => $address)
                            <div class="col-12">
                                <div class="card mx-0">
                                    <div class="card-header" id="addressHeader{{ $key }}">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#addressCollapse{{ $key }}" aria-expanded="false" aria-controls="addressCollapse{{ $key }}">
                                                Address {{ $key + 1 }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="addressCollapse{{ $key }}" class="collapse" aria-labelledby="addressHeader{{ $key }}" data-bs-parent="#addressAccordion" style="color: #000;">
                                        <div class="card-body">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="billing_address_id" value="{{ $address->id }}" id="address{{ $key }}">
                                                <label class="form-check-label" for="address{{ $key }}">
                                                    <strong>Name:</strong> {{ $address->first_name }} {{ $address->last_name }}<br>
                                                    <strong>Email:</strong> {{ $address->email }}<br>
                                                    <strong>Phone:</strong> {{ $address->phone }}<br>
                                                    <strong>Address:</strong> {{ $address->address }}, {{ $address->appartment }}<br>
                                                    <strong>City:</strong> {{ $address->city }}<br>
                                                    <strong>Post Code:</strong> {{ $address->post_code }}<br>
                                                    <strong>Country:</strong> {{ $address->country }}<br>
                                                    <strong>State:</strong> {{ $address->state }}<br>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="create-ac-content">
                        <fieldset>
                            <h2 class="login-title mb-3">Billing details</h2>
                            <div class="row">
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-firstname">First Name <span class="required-f">*</span></label>
                                    <input name="first_name" value="{{ Auth::check() ? Auth::user()->name : old('first_name') }}" id="input-firstname" type="text" autocomplete="first_name" >
                                </div>
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-lastname">Last Name <span class="required-f">*</span></label>
                                    <input name="last_name" value="{{ old('last_name')}}" id="input-lastname" type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-email">E-Mail <span class="required-f">*</span></label>
                                    <input name="email" value="{{ Auth::check() ? Auth::user()->email : old('email') }}" id="input-email" type="email" autocomplete="email" >
                                </div>
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-telephone">Telephone <span class="required-f">*</span></label>
                                    <input name="phone" value="{{ old('phone') }}" id="input-telephone" type="tel">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 required">
                                    <label for="input-address-1">Address <span class="required-f">*</span></label>
                                    <input name="address" value="{{ old('address') }}" id="input-address-1" type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-lg-6 col-xl-6">
                                    <label for="input-address-2">Apartment <span class="required-f">*</span></label>
                                    <input name="appartment" value="{{ old('appartment') }}" id="input-address-2" type="text">
                                </div>
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-city">City <span class="required-f">*</span></label>
                                    <input name="city" value="{{ old('city') }}" id="input-city" type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-postcode">Post Code <span class="required-f">*</span></label>
                                    <input name="post_code" value="{{ old('post_code') }}" id="input-postcode" type="text">
                                </div>
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-country">Country <span class="required-f">*</span></label>
                                    <select name="country" id="input-country">
                                        <option value="Bangladesh">Bangladesh</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                    <label for="input-zone">Region / State <span class="required-f">*</span></label>
                                    <select name="state" id="input-zone">
                                        <option value="">Please Select</option>
                                        <option value="dhaka">Dhaka</option>
                                        <option value="barishal">Barishal</option>
                                        <option value="khulna">Khulna</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 col-lg-12 col-xl-12 mb-0">
                                    <label for="input-company">Order Notes <span class="required-f">*</span></label>
                                    <textarea class="form-control resize-both" rows="3" name="note"></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="your-order-payment">
                        <div class="your-payment">
                            <h2 class="payment-title mb-3">Payment Method</h2>
                            <div class="payment-method">
                                <div class="payment-accordion">
                                    <div id="accordion" class="payment-section">
                                        <div class="card mb-2">
                                            <div class="card-header">
                                                <a class="card-link">Select Payment Option </a>
                                            </div>
                                            <div class="">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="payment_method" value="cash_on_delivery" id="cash_on_delivery" required>
                                                        <label class="form-check-label" for="cash_on_delivery">
                                                            <i class="fa-regular fa-circle-check"></i> Cash On Delivery
                                                        </label>
                                                    </div>

                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="payment_method" value="bkash" id="bkash" required>
                                                        <label class="form-check-label" for="bkash">
                                                            <i class="fa-regular fa-circle-check"></i> Mobile Banking
                                                        </label>
                                                    </div>

                                                    <div id="bkash_form" style="display: none;">
                                                        <!-- Bkash payment form -->
                                                        <h2 class="payment-title mb-3"></h2>
                                                        <label class="payment-title mb-3">Payment Information</label>
                                                            <!-- Bkash number -->
                                                        <div class="form-group">
                                                            <label for="bkash_number">Bkash Number</label>
                                                            <input type="text" class="form-control" id="banking_number" name="banking_number" placeholder="Enter number">
                                                        </div>

                                                        <!-- Amount -->
                                                        <div class="form-group">
                                                            <label for="amount">Amount</label>
                                                            <input type="number" class="form-control" id="amount" name="amount" placeholder="Enter amount">
                                                        </div>

                                                        <!-- Transaction ID -->
                                                        <div class="form-group">
                                                            <label for="transaction_id">Transaction ID</label>
                                                            <input type="text" class="form-control" id="transaction_id" name="transaction_id" placeholder="Enter transaction ID">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="your-payment">
                            <h2 class="payment-title mb-3">Shipping Area</h2>
                            <div class="payment-method">
                                <div class="payment-accordion">
                                    <div id="shipping" class="payment-section">
                                        <div class="card mb-2">
                                            <div class="card-header">
                                                <a class="card-link">Choose Shipping Area </a>
                                            </div>
                                            <div data-parent="#shipping">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="shipping_charge" value="50" id="inside_dhaka" required>
                                                        <label class="form-check-label" for="inside_dhaka">
                                                            <i class="fa-regular fa-circle-check"></i> Inside Dhaka ৳ 50
                                                        </label>
                                                    </div>

                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="shipping_charge" value="100" id="outside_dhaka" required>
                                                        <label class="form-check-label" for="outside_dhaka">
                                                            <i class="fa-regular fa-circle-check"></i> Outside Dhaka ৳ 100
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="your-order">
                            <h2 class="order-title mb-4">Your Order</h2>
                            <div class="table-responsive-sm order-table">
                                <table class="bg-white table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-start">Product Name</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Size</th>
                                            <th>Color</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                            $discount = 0;
                                            $subtotal = 0;
                                        ?>
                                        @foreach ($data['products'] as $product)
                                        @php
                                            $discount += $product['quantity'] * $product['discount'];
                                            $subtotal += $product['quantity'] * $product['price'];
                                        @endphp
                                        <tr>
                                            <td class="text-start">{{$product['name']}}</td>
                                            <td>৳ {{ $product['price'] }}</td>
                                            <td>{{ $product['quantity'] }}</td>
                                            <td>{{ $product['size'] }}</td>
                                            <td>{{ $product['color'] }}</td>
                                            <td>৳ {{ $product['sub_total'] }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot class="font-weight-600">
                                        <tr>
                                            <td colspan="5" class="text-end">Total Product Price </td>
                                            <td>৳ <span id="total_product_price">{{ $subtotal  }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-end">Discount </td>
                                            <td>৳ <span id="discount">{{$discount}}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-end">Shipping </td>
                                            <td>৳ <span id="shipping_amount">0</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-end">Total</td>
                                            <td>৳ <span id="total_amount">{{ $data['total']  }}</span></td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <div class="order-button-payment">
                                    <button type="submit" class="btn {{ Auth::check() ? '' : 'disabled'}}">{{ Auth::check() ? 'Place order' : 'Login First'}}</button>
                                </div>
                            </div>
                        </div>

                        <hr>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    const cashOnDeliveryRadio = document.getElementById('cash_on_delivery');
    const bkashRadio = document.getElementById('bkash');
    const bkashFormDiv = document.getElementById('bkash_form');
    cashOnDeliveryRadio.addEventListener('change', () => {
        bkashFormDiv.style.display = 'none';
    });

    bkashRadio.addEventListener('change', () => {
        bkashFormDiv.style.display = 'block';
    });


    const shippingRadios = document.querySelectorAll('input[name="shipping_charge"]');
    shippingRadios.forEach(function (radio) {
        radio.addEventListener('change', function () {
            const selectedShipping = parseInt(this.value);

            const shippingAmountSpan = document.getElementById('shipping_amount');
            shippingAmountSpan.textContent = selectedShipping;

            const total_product_price = document.getElementById('total_product_price');
            const totalAmountSpan = document.getElementById('total_amount');
            const currentTotal = parseInt(total_product_price.textContent);
            const newTotal = currentTotal + selectedShipping;
            totalAmountSpan.textContent = newTotal;
        });
    });
</script>

<style>

    .form-group {
        margin-bottom: 15px;
    }

    .form-check-input[type="radio"] {
        display: none;
    }

    .form-check-label {
        position: relative;
        padding-left: 30px;
        cursor: pointer;
    }

    .custom-radio-icon {
        position: absolute;
        top: 2px;
        left: 0;
        width: 20px;
        height: 20px;
        background-color: #fff;
        border: 2px solid #007bff;
        border-radius: 50%;
        display: none;
    }

    .form-check-input[type="radio"]:checked + .form-check-label .custom-radio-icon {
        display: block;
    }

    .form-check-input[type="radio"]:checked + .form-check-label {
        font-weight: bold;
        color: #007bff;
    }
</style>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Add event listener for address section buttons
        document.querySelectorAll('.card-header button').forEach(function (button) {
            button.addEventListener('click', function () {
                // Collapse all other address sections when a new one is opened
                document.querySelectorAll('.card .collapse.show').forEach(function (collapse) {
                    collapse.classList.remove('show');
                });
            });
        });
    });
</script>


@endsection
