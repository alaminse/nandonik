@extends('layouts.frontend')
@section('content')
@section('front_title', 'Offer')
<div id="page-content">
    <!-- Collection Banner -->
    <div class="collection-header">
        <div class="collection-hero">
            <div class="collection-hero__image blur-up lazyload">
            </div>
            <div class="collection-hero__title-wrapper">
                <h1 class="collection-hero__title page-width">Offer Page</h1>
            </div>
        </div>
        <div class="collection-description container"></div>
    </div>
    <!-- End Collection Banner -->

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="feature-row">
                    <div class="col-12 col-sm-12 col-md-4 feature-row__item feature-row__text text-left">
                        <div class="row-text">
                            <h2 class="h2">{{$data['offer']->title ?? ''}}</h2>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-7 feature-row__item text-center">
                        <img class="blur-up lazyload" data-src="{{getImageUrl($data['offer']->image)}}"
                            src="{{getImageUrl($data['offer']->image)}}" alt="Leather Bags"
                            title="Leather Bags" />
                    </div>
                </div>
                <p class="py-4">{!! $data['offer']->description !!}</p>
            </div>
            <div class="col-12">
                <div class="productList">
                    <div class="grid-products grid--view-items product-three-load-more">
                        <div class="row">
                            <h3 class="text-center py-3">Offered Products</h3>
                            @if ($data['products']->isEmpty())
                               <div class="alert alert-warning text-danger text-center h3" >Product not found.</div>
                            @else
                                @foreach ($data['products'] as $product)
                                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 item">
                                        <div class="col-12 item">
                                            <div class="product-image">
                                                <a href="{{ route('offer.product', ['product' => $product->slug, 'offer' => $data['offer']->slug])}}">
                                                    <img class="primary blur-up lazyload" data-src="{{ getImageUrl($product->image) }}" alt="{{$product->name}}" title="{{$product->name}}" />
                                                    <img class="hover blur-up lazyload" data-src="{{ getImageUrl($product->thumbnail) }}" alt="{{$product->name}}" title="{{$product->name}}" />
                                                    <div class="product-labels rectangular"><span class="lbl on-sale"> - {{ $product['discount_type'] === 'percent' ? $product['discount'] .'%' : $product['discount'] .' ৳'}} </span> <span class="lbl pr-label1">new</span></div>
                                                </a>
                                                @if(strtotime($data['offer']->offer_end) > time())
                                                    <div class="saleTime desktop" data-countdown="{{ discountDate($data['offer']->offer_end) }}"></div>
                                                @endif

                                                <div class="addtocart btnicontext style6">
                                                    <a href="{{ route('offer.product', ['product' => $product->slug,'offer' => $data['offer']->slug])}}"  class="btn btn-addto-cart">
                                                        <i class="icon an an-shopping-bag"></i>Add to Cart
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-details text-center">
                                                <div class="product-name">
                                                    <a href="{{ route('offer.product', ['product' => $product->slug, 'offer' => $data['offer']->slug])}}">{{$product->name}}</a>
                                                </div>
                                                <div class="grid-view-item__vendor transform-none"></div>
                                                <div class="product-price">
                                                    @if ($product->price > 0)

                                                    @unless ($product['discount'] == 0 || discountCalculation($product) <= 0)
                                                        <span class="old-price">৳ {{ $product->price }}</span>
                                                        <span class="price">৳ {{ discountCalculation($product) }}</span>
                                                    @else
                                                        <span class="price">৳ {{ $product->price }}</span>
                                                    @endunless
                                                    @endif
                                                </div>

                                                <ul class="swatches">
                                                    @foreach ($product->images as $image)
                                                        <li class="swatch medium rounded"><img src="{{ getImageUrl($image->image) }}" alt="{{$product->name}}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$product->name}}" /></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <!-- End Grid Product -->
                </div>

                <!-- Infinit Pagination -->
                {{-- <div class="infinitpaginOuter">
                    <div class="infinitpagin-three">
                        <a href="#" class="btn loadMoreThree">Load More</a>
                    </div>
                </div> --}}
                <!-- End Infinit Pagination -->
            </div>
        </div>
    </div>
</div>
@endsection
