@extends('layouts.frontend')
@section('front_title', 'Invoice')
@section('content')
<!-- Body Content -->
<div id="page-content">
    <!-- Page Title -->
    <div class="page section-header text-center mb-0">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-title">Checkout Success</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->
    <!-- Breadcrumbs -->
    <div class="bredcrumbWrap bredcrumb-style2">
        <div class="container breadcrumbs">
            <a href="{{ url('/') }}" title="Back to the home page">Home</a><span aria-hidden="true">|</span><span
                class="title-bold">Checkout Success</span>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <div class="container checkout-success-content">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="card border-0 rounded-0">
                    <div class="card-body text-center">
                        <p class="checkout-success-icon"><i class="icon an an-check-square"></i></p>
                        <h4 class="card-title">Thank you for your purchase!</h4>
                        <p class="card-text mb-1">Your order # is: <b>{{$order->order_id}}</b>.</p>
                        <p class="card-text mb-1">You will receive an order confirmation email with details of
                            your order and a link to track its progress.</p>
                        <p class="card-text mb-1">All necessary information about the delivery, we sent to your
                            email</p>
                        <a href="{{'/'}}" class="btn btn-primary mt-3">Continue Shopping</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="checkout-item-ordered">
                    <h2>Item Ordered</h2>
                    <div class="table-content table-responsive checkout-review mb-4">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center fw-bold"></th>
                                    <th class="text-start fw-bold">Product Name</th>
                                    <th class="text-center fw-bold">Qty</th>
                                    <th class="text-center fw-bold">Price</th>
                                    <th class="text-center fw-bold">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->products as $product)
                                <tr>
                                    <td class="pro-img text-center"><a href=""><img
                                                class="primary blur-up lazyload"
                                                data-src="{{getImageUrl($product->product->image)}}"
                                                src="{{getImageUrl($product->product->image)}}"
                                                alt="image" title="product" width="80" /></a></td>
                                    <td class="pro-name text-center text-sm-start">
                                        <p class="mb-1"><a href="#">{{$product->product->name}}</a></p>
                                    </td>
                                    <td class="pro-price text-center">{{$product->quantity}}</td>
                                    <td class="pro-price text-center">{{$product->price}}</td>
                                    <td class="pro-total text-center">{{ $product->sub_total }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot class="text-right">
                                <tr>
                                    <td colspan="4" class="item subtotal fw-bold">Subtotal:</td>
                                    <td class="last">{{$order->products_price}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="item discount fw-bold">Discount:</td>
                                    <td class="last">{{ $order->discount }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="item shipping fw-bold">Shipping:</td>
                                    <td class="last">{{$order->shipping_charge}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="item total fw-bold">Grand Total:</td>
                                    <td class="last">{{ $order->total}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="ship-info-details shipping-method-details">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="shipping-details mb-4 mb-sm-0 clearfix">
                                    <h3>Shipping Address</h3>
                                    <p><strong>Email: </strong>{{ $order->billing->email ?? $order->billing->billingAddress->email }},</p>
                                    <p><strong>Phone: </strong>{{ $order->billing->phone ?? $order->billing->billingAddress->phone }},</p>
                                    <p><strong>Address: </strong>{{ $order->billing->address ?? $order->billing->billingAddress->address }},</p>
                                    <p><strong>Appartment: </strong>{{ $order->billing->appartment ?? $order->billing->billingAddress->appartment }},</p>
                                    <p><strong>Post_code: </strong>{{ $order->billing->post_code ?? $order->billing->billingAddress->post_code }}</p>
                                    <p><strong>City: </strong>{{ $order->billing->city ?? $order->billing->billingAddress->city }}</p>
                                    <p><strong>State: </strong>{{ $order->billing->state ?? $order->billing->billingAddress->state }}</p>
                                    <p><strong>Country: </strong>{{ $order->billing->country ?? $order->billing->billingAddress->country }}</p>
                                    <p><strong>Note: </strong>{{ $order->billing->note ?? $order->billing->billingAddress->note }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="billing-details clearfix">
                                    <h3>Shipping Method</h3>
                                    <p>{{ $order->shipping_area }} - {{ $order->shipping_charge }}</p>
                                </div>
                                <div class="billing-details clearfix">
                                    <h3>Payment Method</h3>
                                    <p>{{ $order->payment->payment_method }}</p>
                                    <p>{{ $order->payment->banking_name != null ? 'Banking Name: ' . $order->payment->banking_name : '' }}</p>
                                    <p>{{ $order->payment->banking_number != null ? 'Banking Number: ' . $order->payment->banking_number : '' }}</p>
                                    <p>{{ $order->payment->amount != null ? 'Amount: ' . $order->payment->amount : ''}}</p>
                                    <p>{{ $order->payment->transaction_id != null ? 'Transaction Id: ' . $order->payment->transaction_id : '' }}</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Body Content -->
@endsection
