@extends('layouts.frontend')
@section('front_title', 'Shipping Policy')
@section('content')
    <!-- Body Content -->
    <div id="page-content">
        <!-- Page Title -->
        <div class="login page section-header text-center mb-0">
            <div class="page-title">
                <div class="wrapper"><h1 class="page-width">Shipping and Return Policy</h1></div>
            </div>
        </div>
        <!-- End Page Title -->

        <!-- Body Content -->
        <div class="container text-content">
           {!! $value !!}
        </div>
        <!-- End Body Content -->
    </div>
    <!-- End Body Content -->
@endsection
