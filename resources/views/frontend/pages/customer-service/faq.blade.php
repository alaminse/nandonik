@extends('layouts.frontend')
@section('front_title', 'FAQS')
@section('content')
<!-- Body Content -->
<div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-0">
        <div class="page-title">
            <div class="wrapper"><h1 class="page-width">FAQ's</h1></div>
        </div>
    </div>
    <!-- End Page Title -->

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                {!! $value !!}
            </div>
        </div>
    </div>
</div>
<!-- End Body Content -->
 @endsection
