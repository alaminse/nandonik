@extends('layouts.frontend')
@section('front_title', 'Contact')
@section('content')
 <!-- Body Content -->
 <div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-0">
        <div class="page-title">
            <div class="wrapper"><h1 class="page-width">Contact Us </h1></div>
        </div>
    </div>
    <!-- End Page Title -->
    <div class="container-fluid px-0">
        <div class="row g-0">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0 ">
                <div class="map-section map">
                    <iframe src="{{ $value->map }}" allowfullscreen="" height="420"></iframe>

                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-6 justify-content-center align-items-center flex-wrap px-3 px-sm-5 pt-4 pb-2 mb-md-5 mb-lg-0 mb-sm-5 mb-5">
                <h2 class="text-center">DROP US A LINE</h2>
                <p class="text-center">Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500 </p>

                <!-- Contact Form -->
                <div class="formFeilds contact-form form-vertical">
                    <form action="{{ route('send.message') }}"  method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="name" class="form-control" required placeholder="Name" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="email"  name="email" class="form-control" required placeholder="Email" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input class="form-control" type="tel"  name="phone" required pattern="[0-9\-]*" placeholder="Phone Number"  />
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="subject" class="form-control" required placeholder="Subject" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <textarea  name="message" class="form-control" rows="4" required placeholder="Your Message..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group mailsendbtn mb-0">
                                    {{-- <input class="btn btn-primary" type="submit" name="contactus" value="Send Message" /> --}}
                                    {{-- Problem with class and can't submit data using  class="btn btn-primary"--}}
                                    <button  type="submit"class="custom-button"  value="submit">Send Us a Message </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="response-msg"></div>
                </div>
                <!-- End Contact Form -->
            </div>
        </div>
    </div>
</div>
<!-- End Body Content -->
@endsection
