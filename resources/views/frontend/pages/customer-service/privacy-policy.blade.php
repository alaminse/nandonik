@extends('layouts.frontend')
@section('front_title', 'Privacy Policy')
@section('content')
    <!-- Body Content -->
    <div id="page-content">
        <!-- Page Title -->
        <div class="login page section-header text-center">
            <div class="page-title">
                <div class="wrapper"><h1 class="page-title">{{ $page_title }}</h1></div>
            </div>
        </div>
        <!-- End Page Title -->

        <div class="container text-content">
            <p>{!! $value !!}</p>
        </div>
    </div>
    <!-- End Body Content -->
@endsection
