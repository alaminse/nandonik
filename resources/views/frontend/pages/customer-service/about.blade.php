@extends('layouts.frontend')
@section('front_title', 'About')
@section('content')
 <!-- Body Content -->
 <div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-0">
        <div class="page-title">
            <div class="wrapper"><h1 class="page-width">{{ $page_title }}</h1></div>
        </div>
    </div>
    <!-- End Page Title -->

    <div class="container">
        <div class="row mt-3">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <p>{!! $value !!}</p>
            </div>
        </div>
    </div>
</div>
<!-- End Body Content -->
@endsection
