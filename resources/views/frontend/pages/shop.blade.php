@extends('layouts.frontend')
@section('content')
@section('front_title', 'Shop')
<div id="page-content">
    <!-- Collection Banner -->
    <div class="collection-header">
        <div class="collection-hero">
            {{-- style="background-image:url('frontend/assets/images/collection/women-collection-bnr.jpg');" --}}
            <div class="collection-hero__image blur-up lazyload">
            </div>
            <div class="collection-hero__title-wrapper">
                <h1 class="collection-hero__title page-width">Shop Page</h1>
            </div>
        </div>
        <div class="collection-description container"></div>
    </div>
    <!-- End Collection Banner -->

    <div class="container">
        <div class="row">
                <!-- Sidebar -->
                @include('frontend.pages.product-view.sidebar')
            <div class="col-sm-12 col-md-8 col-lg-9 main-col">
                <div class="productList">
                    <div class="grid-products grid--view-items product-three-load-more">
                        <div class="row">

                            @if ($data['products']->isEmpty())
                               <div class="alert alert-warning text-danger text-center h3" >Product not found.</div>
                            @else
                                @foreach ($data['products'] as $product)
                                      <div class="col-6 col-sm-6 col-md-3 col-lg-3 item">
                                         @include('frontend.includes.product')
                                       </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <!-- End Grid Product -->
                </div>

                <!-- Infinit Pagination -->
                {{-- <div class="infinitpaginOuter">
                    <div class="infinitpagin-three">
                        <a href="#" class="btn loadMoreThree">Load More</a>
                    </div>
                </div> --}}
                <!-- End Infinit Pagination -->
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>

    <script>

        var $data = @json($data);
        let price_min = {{ $data['price_min'] !== '' ? $data['price_min'] : 0 }};
        let price_max = {{ $data['price_max'] !== '' ? $data['price_max'] : 1000 }};

        rangeSlider = $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: 0,
            max: 10000,
            from: price_min,
            step: 10,
            to: price_max,
            grid: true,
            onFinish: function() {
                apply_filters();
            }
        });

        let slider = $(".js-range-slider").data('ionRangeSlider');

        $('.brand-label, .color-label, .size-label').on('change click', function() {
            apply_filters();
        });

        function apply_filters() {
            let brands = [];
            let color = null;
            let size = null;

            $('.brand-label').each(function(){
                if($(this).is(':checked') == true) {
                    brands.push($(this).val());
                }
            })

            $('.color-label').each(function() {
                if ($(this).hasClass('checked')) {
                    color = $(this).data('id');
                }
            });

            $('.size-label').each(function() {
                if ($(this).hasClass('checked')) {
                    size = $(this).data('id');
                }
            });

            let url = '{{ url()->current() }}';
            if(slider.result.from !== null)
            {
                url += '?&price_min='+slider.result.from + '&price_max='+slider.result.to;
            }
            if (color !== null) {
                url += '?&color=' + color.toString();
            }

            if (size !== null) {
                url += '?&size=' + size.toString();
            }

            if (brands.length > 0) {
                url += '?&brand=' + brands.toString();
            }


            window.location.href = url;
        }
    </script>
@endpush

@endsection
