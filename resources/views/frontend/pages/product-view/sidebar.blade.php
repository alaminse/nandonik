
<style>
    .filterBox ul:not(.filter-color) input[type="checkbox"]:checked + label > span:before {
        border-color: transparent transparent #ffffff #ffffff;
    }

    .text-active {
        color: #f19280;
    }
</style>

<div class="col-12 col-sm-12 col-md-4 col-lg-3 sidebar filterbar">
    <div class="closeFilter d-block d-md-block d-lg-none"><i class="icon an an-times"></i></div>
    <div class="sidebar_tags">
        <!-- Categories -->
        <div class="sidebar_widget filterBox categories filter-widget">
            <div class="widget-title">
                <h2>Categories</h2>
            </div>
            <div class="widget-content">
                <ul class="sidebar_categories">
                    @foreach (categories() as $category)
                        <li class="level1 sub-level">
                            <a href="{{ count($category->sub_categories) == 0 ? route('filter', [$category->slug]) : '#' }}" class="site-nav {{$data['category_selected'] == $category->id ? 'active text-active' : ''}}">
                                {{ $category->name }}
                            </a>
                            @if ($category->sub_categories)
                                <ul class="sublinks" style="display: {{ count($category->sub_categories) && $data['category_selected'] == $category->id ? 'block' : 'none' }};">
                                    @foreach ($category->sub_categories as $sub_category)
                                        <li class="level2">
                                            <a href="{{ route('filter', [$category->slug, $sub_category->slug]) }}" class="site-nav {{$data['sub_category_selected'] == $sub_category->id ? 'active text-active' : ''}}">
                                                {{ $sub_category->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- End Categories -->

        <!-- Price Filter -->
        <div class="sidebar_widget filterBox filter-widget">
            <div class="widget-title">
                <h2>Price</h2>
            </div>
            <input type="text" class="js-range-slider" name="my_range" value="" />
        </div>
        <!-- End Price Filter -->

        <!-- Size Swatches -->
        <div class="sidebar_widget filterBox filter-widget size-swacthes">
            <div class="widget-title">
                <h2>Size</h2>
            </div>
            <div class="filter-color swacth-list widget-content clearfix">
                @foreach (sizes() as $size)
                    <span class="size-label swacth-btn {{$size->name}} {{($data['size'] == $size->name) ? 'checked' : ''}}" data-bs-toggle="tooltip" data-bs-placement="top"
                    title="{{$size->name}}" data-id="{{$size->name}}">{{$size->name}}</span>
                @endforeach
            </div>
        </div>
        <!-- End Size Swatches -->

        <!-- Color Swatches -->
        <div class="sidebar_widget filterBox filter-widget">
            <div class="widget-title">
                <h2>Color</h2>
            </div>
            <div class="filter-color swacth-list widget-content clearfix">
                @foreach (colors() as $color)
                    <span class="color-label swacth-btn {{$color->slug }} {{($data['color'] == $color->slug) ? 'checked' : ''}}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$color->name}}"  data-id="{{$color->slug}}"></span>
                @endforeach
            </div>
        </div>
        <!-- End Color Swatches -->

        <!-- Vendors -->
        <div class="sidebar_widget filterBox filter-widget">
            <div class="widget-title">
                <h2>Brands</h2>
            </div>
            <div class="widget-content">
                <ul class="widget-vendors">
                    @foreach (brands() as $brand)
                    <li>
                        <input class="brand-label" type="checkbox" value="{{$brand->slug}}" id="{{ $brand->slug }}" {{in_array($brand->slug, $data['brands']) ? 'checked' : ''}}/>
                        <label  for="{{ $brand->slug }}"><span><span></span></span>{{$brand->name}}</label>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
