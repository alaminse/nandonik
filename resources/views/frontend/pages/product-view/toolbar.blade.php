<div class="toolbar">
    <div class="filters-toolbar-wrapper">
        <div class="row">
            <div
                class="col-4 col-md-4 col-lg-4 filters-toolbar__item collection-view-as d-flex justify-content-Start align-items-center">
                <button type="button"
                    class="btn-filter d-block d-md-block d-lg-none icon an an-sliders-h"
                    data-bs-toggle="tooltip" data-bs-placement="top"
                    title="Filters"></button>
                <a href="#" class="change-view change-view--active"
                    data-bs-toggle="tooltip" data-bs-placement="top" title="Grid View">
                    <i class="icon an an-table"></i>
                </a>
            </div>
            <div
                class="col-4 col-md-4 col-lg-4 text-center filters-toolbar__item filters-toolbar__item--count d-flex justify-content-center align-items-center">
                <span class="filters-toolbar__product-count">Showing: 22</span>
            </div>
            <!-- Filter for sorting -->
            <div
                class="col-4 col-md-4 col-lg-4 d-flex justify-content-end align-items-center text-end">
                <div class="filters-toolbar__item">
                    <label for="SortBy" class="hidden">Sort</label>
                    <select name="SortBy" id="SortBy"
                        class="filters-toolbar__input filters-toolbar__input--sort">
                        <option value="title-ascending" selected="selected">Sort</option>
                        <option>Best Selling</option>
                        <option>Alphabetically, A-Z</option>
                        <option>Alphabetically, Z-A</option>
                        <option>Price, low to high</option>
                        <option>Price, high to low</option>
                        <option>Date, new to old</option>
                        <option>Date, old to new</option>
                    </select>
                    <input class="collection-header__default-sort" type="hidden"
                        value="manual">
                </div>
            </div>
        </div>
    </div>
</div>