<div class="col-6 col-sm-6 col-md-4 col-lg-4 item">
    <!-- Product Image -->
    <div class="product-image">
        <!-- Product Image -->
        <a href="#">
            <!-- Image -->
            <img class="primary blur-up lazyload"
                data-src="{{asset('frontend/assets/images/product-images/product-image1.jpg')}}"
                src="{{asset('frontend/assets/images/product-images/product-image1.jpg')}}"
                alt="image" title="product" />
            <!-- End Image -->
            <!-- Hover Image -->
            <img class="hover blur-up lazyload"
                data-src="{{asset('frontend/assets/images/product-images/product-image1-1.jpg')}}"
                src="{{asset('frontend/assets/images/product-images/product-image1-1.jpg')}}"
                alt="image" title="product" />
            <!-- End Hover Image -->
            <!-- Product Label -->
            <div class="product-labels rectangular"><span
                    class="lbl on-sale">Exclusive</span></div>
            <!-- End Product Label -->
        </a>
        <!-- End Product Image -->

        <!-- Countdown -->
        <div class="saleTime desktop" data-countdown="2022/03/01"></div>
        <!-- End Countdown -->

        <!-- Product Button -->
        <div class="button-set">
            <div class="quickview-btn" data-bs-toggle="tooltip"
                data-bs-placement="top" title="quick view">
                <a href="#open-quickview-popup"
                    class="btn quick-view-popup quick-view"><i
                        class="icon an an-search"></i></a>
            </div>
            <div class="variants add" data-bs-toggle="tooltip"
                data-bs-placement="top" title="add to cart">
                <form class="addtocart" action="#" method="post">
                    <a href="#open-addtocart-popup"
                        class="btn cartIcon btn-addto-cart open-addtocart-popup"><i
                            class="icon an an-shopping-bag"></i></a>
                </form>
            </div>
        </div>
        <!-- End Product Button -->
    </div>
    <!-- End Product Image -->
    <!-- Product Details -->
    <div class="product-details text-center">
        <!-- Product Name -->
        <div class="product-name">
            <a href="#">Edna Dress</a>
        </div>
        <!-- End Product Name -->
        <!-- Product Price -->
        <div class="product-price">
            <span class="old-price">$500.00</span>
            <span class="price">$600.00</span>
        </div>
        <!-- End Product Price -->
        <!-- Product Review -->
        <div class="product-review">
            <i class="an an-star"></i>
            <i class="an an-star"></i>
            <i class="an an-star"></i>
            <i class="an an-star"></i>
            <i class="an an-star-half-alt"></i>
        </div>
        <!-- End Product Review -->
        <!-- Variant -->
        <ul class="swatches">
            <li class="swatch large radius"><img
                    src="{{asset('frontend/assets/images/product-images/variant1.jpg')}}"
                    alt="image" data-bs-toggle="tooltip" data-bs-placement="top"
                    title="Blue" />
            </li>
            <li class="swatch large radius"><img
                    src="{{asset('frontend/assets/images/product-images/variant1.jpg')}}"
                    alt="image" data-bs-toggle="tooltip" data-bs-placement="top"
                    title="Maroon" /></li>
            <li class="swatch large radius"><img
                    src="{{asset('frontend/assets/images/product-images/variant1.jpg')}}"
                    alt="image" data-bs-toggle="tooltip" data-bs-placement="top"
                    title="Pink" />
            </li>
            <li class="swatch large radius"><img
                    src="{{asset('frontend/assets/images/product-images/variant1.jpg')}}"
                    alt="image" data-bs-toggle="tooltip" data-bs-placement="top"
                    title="Green" /></li>
        </ul>
        <!-- End Variant -->
    </div>
    <!-- End Product Details -->
    <!-- Countdown -->
    <div class="timermobile">
        <div class="saleTime desktop" data-countdown="2022/03/01"></div>
    </div>
    <!-- End Countdown -->
</div>
