@extends('layouts.frontend')
@section('front_title', 'Forgot Password')
@section('content')
<div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-4">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-width">Forgot Your Password</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->
    <div class="container">
        <div class="row">
            <!-- Main Content -->
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 box offset-md-3">
                <div class="mb-4">
                    <form method="post" action="#" accept-charset="UTF-8" class="customer-form">
                        <h3>Retrieve your password here</h3>
                        <p>Please enter your email address below. You will receive a link to reset your password.</p>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerEmail">Email Address <span class="required">*</span></label>
                                    <input type="email" name="customer[email]" placeholder="" id="CustomerEmail"
                                        autofocus />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-left col-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn mb-3" value="Submit">
                                <p class="mb-4">
                                    <a href="{{route('login')}}">« Back To Login Page</a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Main Content -->
        </div>
    </div>
</div>
@endsection
