@extends('layouts.frontend')
@section('front_title', 'Product Details')
@section('content')
    <style>
        .size-options {
            display: flex;
            flex-wrap: wrap;
            margin-top: 5px;
        }

        .swatch-element.lxxx {
            margin-right: 5px;
            margin-bottom: 5px;
        }
    </style>
    <!-- Body Content -->
    <div id="page-content">

        <div class="container pt-5">
            <!-- Main Content -->
            <div id="ProductSection-product-template" class="product-template__container prstyle2">
                <!-- #ProductSection product template -->
                <div class="product-single product-single-1">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">

                            <div class="product-details-img product-single__photos bottom">
                                <!-- Main Product Image with ZoomPro -->
                                <div class="zoompro-wrap product-zoom-right pl-20">
                                    <div class="zoompro-span" id="variant_image">
                                        <img class="blur-up lazyload" alt="" src="{{ getImageUrl($product->image) }}" height="550px" width="400px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                            <div class="product-info">
                                @if ($product->stock >= 1)
                                <p class="product-stock">Availability:
                                        <span class="instock"> <strong class="text-success">In Stock</strong></span>
                                    @else
                                        <span class="outstock"><strong class="text-danger">Unavailable</strong></span>
                                </p>
                                @endif

                                <p class="product-type">Product Type: <span>unique</span></p>
                                <p class="product-type">Brand: <span>{{ $product->brand->name }}</span></p>
                                <p class="product-type">Category: <span>{{ $product->category->name }}</span></p>
                                <p class="product-type">Sub Category: <span>{{ $product->sub_category->name }}</span></p>
                            </div>
                            <div class="product-single__meta mt-3">
                                <h1 class="product-single__title">{{ $product->name }}</h1>
                                <!-- Product Price -->
                                <div class="product-single__price product-single__price-product-template">
                                    <span class="">Price: </span>
                                    @unless ($product['discount'] == 0 || discountCalculation($product) <= 0)
                                        <span class="old-price"><del class="text-danger">৳ {{ $product->price }}</del></span>
                                        <span class="price">৳ {{ discountCalculation($product) }}</span>
                                        <input type="hidden" name="price" value="{{ discountCalculation($product) }}">
                                    @else
                                        <input type="hidden" name="price" value="{{$product->price}}">
                                        <span class="price">৳ {{ $product->price }}</span>
                                    @endunless

                                </div>
                                <div class="saleTime product-countdown style3 pb-1"
                                    data-countdown="{{ $product->discount_end }}"></div>
                                <!-- End Product Price -->
                                <!-- Form -->
                                <form method="POST" action="{{ route('addTo.cart', $product->slug) }}"
                                    id="product_form_10508262282" accept-charset="UTF-8"
                                    class="product-form product-form-product-template product-form-border hidedropdown"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="swatch clearfix swatch-0 option1 w-100" data-option-index="0">
                                        <div class="product-form__item">

                                            <div class="color-options">
                                                <label>Color:</label>
                                                @foreach ($product->colors as $color)
                                                    <div data-value="{{ $color->id }}"
                                                        class="swatch-element color available">
                                                        <input class="swatchInput" id="swatch-0-{{ $color->id }}"
                                                            type="radio" name="color" value="{{ $color->id }}">
                                                        <label style="background-color: {{ $color->color->code }}"
                                                            class="swatchLbl medium" for="swatch-0-{{ $color->id }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                                            title="{{ $color->color->name }}"></label>
                                                    </div>
                                                @endforeach
                                            </div>

                                            <div class="size-options" id="size-options">

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Image Swatch -->
                                    <div class="product-action clearfix">
                                        <div class="product-form__item--quantity">
                                            <div class="wrapQtyBtn">
                                                <div class="qtyField">
                                                    <a class="qtyBtn minus" href="javascript:void(0);"><i
                                                            class="icon an an-minus" aria-hidden="true"></i></a>
                                                    <input type="text" name="quantity" value="1"
                                                        class="product-form__input qty" />
                                                    <a class="qtyBtn plus" href="javascript:void(0);"><i
                                                            class="icon an an-plus" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="product-form__item--submit">
                                            <button type="submit" class="btn product-form__cart-submit">Add to
                                                Cart</button>
                                        </div>
                                    </div>
                                    <div class="lightboximages">
                                        @foreach ($product->images as $item)
                                            <a href="{{ getImageUrl($item->image) }}" data-size="1462x2048"></a>
                                        @endforeach
                                    </div>
                                </form>

                                <!-- Product Thumbnails with Slick Slider -->
                                <div class="product-thumb product-thumb-1">
                                    <div id="gallery" class="product-dec-slider-1 product-tab-left">
                                        @foreach ($product->images as $item)
                                          <a href="#" data-image="{{ getImageUrl($item->image) }}" class="thumbnail" onclick="handleImageClick('{{ getImageUrl($item->image) }}')">
                                            <img class="blur-up lazyload p-1" src="{{ getImageUrl($item->image) }}" alt="{{ $product->name }}" />
                                          </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- End Product Info -->
                        </div>
                    </div>
                    <!-- End Product single -->

                    <!-- Product Tabs -->
                    <div class="tabs-listing tab-details mt-0 mt-md-4 mt-4">
                        <h1 class="product-single__title">Product Details</h1>
                        <div class="product-single__description rte">
                            <p class="mb-2">{!! $product->description !!}</p>
                        </div>
                        <!-- End Tabs Container -->
                    </div>
                    <!-- End Product Tabs -->

                    <!-- Related Product Slider -->
                    <div class="related-product grid-products">
                        <header class="section-header">
                            <h2 class="section-header__title text-center h2"><span>Related Products</span></h2>
                            <p class="sub-heading">You can stop autoplay, increase/decrease aniamtion speed and
                                number of grid to show and products from store admin.</p>
                        </header>
                        <div class="productPageSlider">
                            @foreach ($product['relatedProducts'] as $produt)
                                @include('frontend.includes.product')
                            @endforeach
                        </div>
                    </div>
                    <!-- End Related Product Slider -->
                </div>
                <!-- #ProductSection product template -->
            </div>
            <!-- End Main Content -->
        </div>
    </div>

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script>
            $(document).ready(function() {
                // Event handler for color change
                $('input[name="color"]').on('change', function() {
                    let productColorId = $(this).val();
                    $.ajax({
                        type: 'GET',
                        url: '/get-sizes/' + productColorId,
                        success: function(data) {
                            var sizeOptions = $('#size-options');

                            sizeOptions.empty();
                            data.forEach(function(size) {
                                sizeOptions.append(
                                    '<div class="swatch-element available">\
                                        <input class="swatchInput" id="swatch-1-' + size.id + '" type="radio" name="color_size" value="' + size.id + '" >\
                                        <label class="swatchLbl color" for="swatch-1-' + size.id + '" data-bs-toggle="tooltip" data-bs-placement="top" title="' + size.size.name + '">' + size.size.name + '</label>\
                                    </div>'
                                );
                            });
                        },
                        error: function() {
                            console.log('Error fetching sizes');
                        }
                    });
                });
            });

            $(document).ready(function() {
                $(document).on('change', 'input[name="color_size"]', function() {
                    var selectedColorSizeId = $(this).val();
                    $.ajax({
                        type: 'GET',
                        url: '/get-price/' + selectedColorSizeId,
                        success: function(data) {
                            $('#variant_image img').attr('src', data.image).show();
                        },
                        error: function() {
                            console.log('Error fetching price');
                        }
                    });
                });
            });
        </script>
        <script>
            function handleImageClick(imageUrl) {
              $('#variant_image img').attr('src', imageUrl).show();
            }
          </script>
    @endpush
@endsection
