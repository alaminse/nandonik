@extends('layouts.frontend')
@section('front_title', 'Login')
@section('content')
<!-- Body Content -->
<div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-0">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-width">Login</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->
    <div class="container">
        <div class="row mt-4">
            <!-- Main Content -->
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 box mb-4 mb-md-0">
                <h3>New Customers</h3>
                <p>By creating an account with our store, you will be able to move through the checkout process faster,
                    store multiple shipping addresses, view and track your orders in your account and more.</p>
                <a href="{{route('register')}}" class="btn">Create an account</a>
            </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 box">
                    <div class="mb-4">
                        <form method="POST" action="{{ route('login') }}" id="CustomerRegisterForm" class="customer-form">
                        @csrf
                            <h3>Registered Customers</h3>
                            <p>If you have an account with us, please log in.</p>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="email" class="col-md-4 col-form-label">{{ __('Email Address') }}</label>

                                        <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>

                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror


                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-0">
                            <div class="col-md-8 d-flex justify-content-between flex-wrap">
                                <div>
                                    <button type="submit" class="btn mb-3">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                                <div>
                                    @if (Route::has('password.request'))
                                    <a  href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
               </div>
            <!-- End Main Content -->
        </div>
    </div>
</div>
<!-- End Body Content -->
@endsection


