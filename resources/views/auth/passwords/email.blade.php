@extends('layouts.frontend')
@section('content')
<div id="page-content">
    <!-- Page Title -->
    <div class="login page section-header text-center mb-4">
        <div class="page-title">
            <div class="wrapper">
                <h1 class="page-width">Forgot Your Password</h1>
            </div>
        </div>
    </div>
    <!-- End Page Title -->
    <div class="container">
        {{-- Main Content --}}
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 box offset-md-3">
            <div class="mb-4">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <h3>Retrieve your password here</h3>
                            <p>Please enter your email address below. You will receive a link to reset your password.</p>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="email" class="col-md-4 col-form-label">{{ __('Email Address') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
