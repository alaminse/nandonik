<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self PAID()
 * @method static self DUE()
 */
class PaymentStatus extends Enum
{
    protected static function values(): array
    {
        return [
            'PAID' => 0,
            'DUE' => 1,
        ];
    }

    public function title(): string|null
    {
        return match ($this->value) {
            0 => 'Paid',
            1 => 'Due',
            default => null,
        };
    }
    public function message(): string|null
    {
        return match ($this->value) {
            0 => 'success',
            1 => 'info',
            default => null,
        };
    }
}
