<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self ACCEPT()
 * @method static self PENDING()
 * @method static self REJECT()
 */
class CancleRequestStatus extends Enum
{
    protected static function values(): array
    {
        return [
            'ACCEPT' => 0,
            'PENDING' => 1,
            'REJECT' => 2
        ];
    }

    public function title(): string|null
    {
        return match ($this->value) {
            0 => 'Accept',
            1 => 'Pending',
            2 => 'Reject',
            default => null,
        };
    }
    public function message(): string|null
    {
        return match ($this->value) {
            0 => 'success',
            1 => 'warning',
            2 => 'danger',
            default => null,
        };
    }
}
