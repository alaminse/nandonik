<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;
/**
 * @method static self PENDING()
 * @method static self CANCELED()
 * @method static self CONFIRMED()
 * @method static self READYTOSHIP()
 * @method static self OUTFORDELIVERY()
 * @method static self DELIVERED()
 */
class OrderStatus extends Enum
{
    protected static function values(): array
    {
        return [
            'PENDING' => 1,
            'CONFIRMED' => 2,
            'CANCELED' => 3,
            'READYTOSHIP' => 4,
            'OUTFORDELIVERY' => 5,
            'DELIVERED' => 6,
        ];
    }
    public function title(): string|null
    {
        return match ($this->value) {
            1 => 'Pending',
            2 => 'Cancel',
            3 => 'Confirm',
            4 => 'Ready to ship',
            5 => 'Out for Delivery',
            6 => 'Delivered',
            default => null,
        };
    }

    public function message(): string|null
    {
        return match ($this->value) {
            1 => 'warning',
            2 => 'danger',
            3 => 'info',
            4 => 'primary',
            5 => 'secondary',
            6 => 'success',
            default => null,
        };
    }
}
