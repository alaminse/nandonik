<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function billing()
    {
        return $this->hasOne(BillingAddress::class);
    }

    public function products()
    {
        return $this->hasMany(OrderedProduct::class);
    }

    public function cancle_req()
    {
        return $this->hasOne(CancelOrder::class,);
    }
}
