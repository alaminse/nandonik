<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ColorSize extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function product_color(): BelongsTo
    {
        return $this->belongsTo(ProductColor::class);
    }

    public function size(): BelongsTo
    {
        return $this->belongsTo(Size::class);
    }
}
