<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductColor extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function color(): belongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'color_sizes', 'product_color_id', 'size_id');
    }
}
