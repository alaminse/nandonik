<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function relatedProducts()
    {
        return $this->hasMany(Product::class, 'category_id', 'category_id')->where('status', 1);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function colorSize(): HasManyThrough
    {
        return $this->hasManyThrough(
            ColorSize::class,
            ProductColor::class
        );
    }

    public function colors(): HasMany
    {
        return $this->hasMany(ProductColor::class);
    }

}
