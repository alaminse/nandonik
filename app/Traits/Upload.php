<?php

namespace App\Traits;

use File;
use Intervention\Image\Facades\Image;

trait Upload
{
    public function uploadFile($file, $path, $width, $hight)
    {
        if($file){
            $filename = time().rand(1,99).'.'.$file->getClientOriginalExtension();
            $upload_path = "uploads/$path";
            $image = Image::make($file)->resize($width, $hight);
            $real_path = public_path($upload_path).'/'.$filename;
            $image->save($real_path);
            return $path.'/'.$filename;
        }
    }

    public function deleteFile($path)
    {
        if(File::exists(public_path('uploads/'.$path))) {
            File::delete(public_path('uploads/'.$path));
        }
        return true;
    }
}
