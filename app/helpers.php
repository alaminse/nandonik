<?php

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\SiteSetting;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Size;
use Carbon\Carbon;

if (!function_exists('getImageUrl')) {
    function getImageUrl(string $path = null): string
    {
        if ($path == null) return asset('default.jpg');

        return asset('uploads/' . $path);
    }
}

if (!function_exists('discountCalculation')) {
    function discountCalculation($product)
    {
        $price = 0;

        $currentDate = Carbon::now()->format('YmdHis');
        $discountStart = Carbon::parse($product['discount_start'])->format('YmdHis');
        $discountEnd = Carbon::parse($product['discount_end'])->format('YmdHis');

        if ($currentDate >= $discountStart && $currentDate <= $discountEnd) {
            if ($product['discount_type'] === 'flat') {
                $price = $product['discount'];
            } elseif ($product['discount_type'] === 'percent') {
                $price = ($product['price'] * $product['discount'] / 100);
            }
        }

        return $price;
    }
}

if (!function_exists('discountDate')) {
    function discountDate($date)
    {
        $date = \Carbon\Carbon::parse($date)->format('Y/m/d');
        return $date;
    }
}

if (!function_exists('contact')) {
    function contact()
    {
        $value = [];
        $contact = SiteSetting::where('title', 'addresses')->first();
        if(isset($contact->value))
        {
            $value = json_decode($contact->value);
        }

        return $value;
    }
}
if (!function_exists('brands')) {
    function brands()
    {
        return Brand::where('status', 1)->get();
    }
}

if (!function_exists('colors')) {
    function colors()
    {
        return Color::get();
    }
}

if (!function_exists('sizes')) {
    function sizes()
    {
        return Size::get();
    }
}

if (!function_exists('categories')) {
    function categories()
    {
        return Category::where('status', 1)
            ->with(['sub_categories' => function ($query) {
                $query->where('status', 1);
            }])
            ->get();
    }
}

if (!function_exists('nav_menu')) {
    function nav_menu()
    {
        $menu = [];
        $menu['offers'] = Offer::select('title', 'slug')
            ->where('status', 1)
            ->inRandomOrder()
            ->take(10)
            ->get();

        $menu['new_in'] = Product::select('slug', 'name')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        return $menu;
    }
}
