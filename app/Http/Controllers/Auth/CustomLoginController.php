<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Toastr;

class CustomLoginController extends Controller
{

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // $temp_user_id = session()->get('temp_user_id');
            // return session()->get('temp_user_id');
            if (session()->has('temp_user_id')) {
                $temp_user_id = session()->get('temp_user_id');
                $cartProducts = Cart::where('temp_user_id', $temp_user_id)->get();
                foreach ($cartProducts as $product) {
                    $product->update(['user_id' => Auth::user()->id]);
                }
                session()->forget('temp_user_id');
            }
            if (Auth::user()->hasAnyRole(['admin', 'manager'])) {
                return redirect()->route('admin.dashboard');
            }

            Toastr::success('Login Successfully.', 'success', [
                "positionClass" => "toast-top-right",
                "closeButton" => true,
                "progressBar" => true,
            ]);
            return redirect()->back();
        } else {
            Toastr::error('Try Again.', 'error', [
                "positionClass" => "toast-top-right",
                "closeButton" => true,
                "progressBar" => true,
            ]);
            return $this->sendFailedLoginResponse($request);
        }
    }
}

