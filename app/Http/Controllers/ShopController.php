<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\ColorSize;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Size;
use App\Models\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function offer($offer)
    {
        $data = [];
        $data['offer'] = Offer::where('slug', $offer)->first();
        $tagsArray = explode(',', $data['offer']->tags);
        $data['products'] = Product::whereIn('name', $tagsArray)->get();

        return view('frontend.pages.offer', compact('data'));
    }

    public function offer_product($product, $offer)
    {
        $discount = 0;
        $product = Product::where('slug', $product)->first();
        $offer = Offer::where('slug', $offer)->first();
        $product['offer'] = $offer->id;

        return view('frontend.pages.buy_offer', compact('product', 'offer'));
    }

    public function  product_details($slug)
    {
        $product = Product::where('slug', $slug)->with('relatedProducts')->first();
        return view('frontend.pages.product-details', compact('product'));
    }

    public function filter(Request $request, $category = null, $sub_category = null)
    {
        $data = [];
        $data['category_selected'] = '';
        $data['sub_category_selected'] = '';
        $data['brands'] = [];
        $data['size'] = '';
        $data['color'] = '';
        $data['price_min'] = '';
        $data['price_max'] = '';

        $product = Product::where('status', 1)->where('stock', '>', 0);

        if(!empty($request->input('q')))
        {
            $query = str_replace('?', '', $request->input('q'));

            $products = $product->where('name', 'like', '%' . $query . '%')
                ->orWhere('tags', 'like', '%' . $query . '%');
        }

        if(!empty($request->get('color')))
        {
            $data['color'] = str_replace('?', '', $request->get('color'));

            $color = Color::select('id')->where('slug', $data['color'])->first();
            if ($color) {
                $product = $product->whereHas('colors', function ($query) use ($color) {
                    $query->where('colors.id', $color->id);
                });
            }
        }

        if(!empty($request->get('size')))
        {
            $data['size'] = str_replace('?', '', $request->get('size'));

            $size = Size::select('id')->where('name', $data['size'])->first();
            $product = $product->whereHas('colors.sizes', function ($query) use ($size) {
                $query->where('sizes.id', $size->id);
            });
        }

        if(!empty($request->get('brand')))
        {
            $data['brands'] = explode(',', $request->get('brand'));
            $brandIds = Brand::whereIn('slug', $data['brands'])->pluck('id')->toArray();

            $product = $product->whereIn('brand_id', $brandIds);
        }

        if(!empty($category))
        {
            $category = Category::where('slug', $category)->first();
            $product = $product->where('category_id', $category->id);
            $data['category_selected'] = $category->id;
        }

        if(!empty($sub_category))
        {
            $sub_category = SubCategory::where('slug', $sub_category)->first();
            $data['sub_category_selected'] = $sub_category->id;
            $product = $product->where('sub_category_id', $data['sub_category_selected']);
        }

        if($request->get('price_min') != '' && $request->get('price_max') != '')
        {
            $data['price_min'] = intval($request->get('price_min'));
            $data['price_max'] = intval($request->get('price_max'));

            $product = $product->when(method_exists($product, 'colors') && $product->colors()->first()?->sizes(), function ($query) use ($data) {
                $query->whereHas('colors.sizes', function ($subQuery) use ($data) {
                    $subQuery->whereBetween('color_size.price', [$data['price_min'], $data['price_max']]);
                });
            }, function ($query) use ($data) {
                $query->whereBetween('price', [$data['price_min'], $data['price_max']]);
            });
        }

        $data['products'] = $product->latest()->paginate(15);

        return view('frontend.pages.shop', compact('data'));
    }

    public function colorSize($productColor)
    {
        $sizes = ColorSize::where('product_color_id', $productColor)->with('size')->get();
        return response()->json($sizes);
    }

    public function getPrice($colorSizeId)
    {
        $colorSize = ColorSize::find($colorSizeId);
        $price = 0;
        $image = '';

        if ($colorSize) {
            $price = $colorSize->price;
            $image = getImageUrl($colorSize->image);
            return response()->json(['price' => $price, 'image' => $image]);
        }

        return response()->json(['price' => $price, 'image' => $image]);
    }
}
