<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SendMessage;
use Toastr;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $SendMessage= new SendMessage;
        $SendMessage->name     =$request->name;
        $SendMessage->email    =$request->email;
        $SendMessage->phone    =$request->phone;
        $SendMessage->subject  =$request->subject;
        $SendMessage->message  =$request->message;
        Toastr::success('Thank you for valuable comment', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);

        $SendMessage->save();
        return redirect()->back();

    }

    public function delete(string $id){

        $delte_instance = SendMessage::find($id);
        if( !is_null($delte_instance) ){
            $delte_instance->delete();
            return redirect()->back()->with('success', 'Delete Successfully');
        }else{
            //404 page
        }
    }

}
