<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Validator;
use App\Traits\Upload;
use DB;
use Toastr;

class ProfileController extends Controller
{
    public function create(Request $request)
    {
        $validator = $this->dataValidation($request);

        $profile = Profile::create($validator);

        if(!$profile)
        {
            Toastr::error('Profile Not Update.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }

        Toastr::success('Profile Update Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        return redirect()->back();
    }

    private function dataValidation($data)
    {
        $validator = Validator::make($data->all(), [
            'lname'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'country'=>'required',
            'postalcode'=>'required',
            'city'=>'required',
            'state'=>'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();
        $input['user_id'] = auth()->user()->id;
        return $input;
    }

    public function update(Request $request, Profile $profile)
    {
        $validator = $this->dataValidation($request);

        try {
            $profile->update($validator);
            Toastr::success('Profile Update Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors(['error' => 'Try Again!! <br>'. $e->getMessage()]);
        }
    }
}
