<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Models\BillingAddress;
use App\Models\CancelOrder;
use App\Models\Cart;
use App\Models\ColorSize;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderedProduct;
use App\Models\Payment;
use App\Models\Product;
use Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CartController extends Controller
{
    public function find_product(Product $product)
    {
        $product->load('images');
        return response()->json(['product' => $product]);
    }

    public function addToCart(Request $request, $slug)
    {
        $color_size_id = null;
        $validator = Validator::make($request->all(), [
            'quantity' => 'nullable|numeric',
            'color_size' => 'nullable|exists:color_sizes,id'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $validator = $validator->validated();

        $product = Product::where('slug', $slug)->first();

        if(!$product)
        {
            Toastr::success('Product Not Found.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }
        if($product->stock < $validator['quantity'])
        {
            Toastr::success('Not Enough Stock.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }

        $cart = Cart::where('product_id', $product->id);
        $discount = 0;
        $price = $product->price;
        if($product->discount > 0)
        {
            $discount = discountCalculation($product);
        }

        try {
            DB::beginTransaction();

            if($product->hasAttribute == 1)
            {
                if(!isset($validator['color_size']))
                {
                    Toastr::success('Select Color and Size.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                    return redirect()->back();
                }
                $color_size = ColorSize::find($validator['color_size']);

                $color_size_id = $color_size->id;
                if($color_size->stock < $validator['quantity'])
                {
                    Toastr::success('Not Enough Stock..', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                    return redirect()->back();
                }
                $color_size->decrement('stock', $validator['quantity']);
                $color_size->increment('number_of_sell', $validator['quantity']);
            }

            $data = [
                'product_id' => $product->id,
                'color_size' => $color_size_id,
                'quantity' => $validator['quantity'],
                'price' => $price,
                'discount' => $discount,
                'sub_total' => ($price - $discount) * $validator['quantity'],
            ];

            if(Auth::check())
            {
                session()->forget('temp_user_id');
                $data['user_id'] = Auth::user()->id;
                $cart->where('user_id', $data['user_id']);
            } else {

                if (!session()->has('temp_user_id')) {
                    $tempUserId = Str::uuid();
                    session()->put('temp_user_id', $tempUserId);
                }

                $data['temp_user_id'] = session('temp_user_id');
                $cart->where('temp_user_id', $data['temp_user_id']);
            }

            $cart = $cart->first();

            if($cart)
            {
                Toastr::success('Product is already in the cart.', 'info', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                return redirect()->back();
            }

            Cart::create($data);
            $product->decrement('stock', $validator['quantity']);
            $product->increment('number_of_sell', $validator['quantity']);

            DB::commit();
            Toastr::success('Cart Update Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        } catch (\Exception $e) {

            DB::rollBack();
            Toastr::success($e->getMessage(), 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        }
    }

    public function cart_count()
    {
        $cartCount = Cart::query();
        if(Auth::check())
        {
            $user_id = Auth::user()->id;
            $cartCount = $cartCount->where('user_id', $user_id)->count();
        } else if (session()->has('temp_user_id')) {
            $temp_user_id = session()->get('temp_user_id');
            $cartCount = $cartCount->where('temp_user_id', $temp_user_id)->count();
        } else {
            $cartCount = 0;
        }
        return response()->json(['cartCount' => $cartCount]);
    }

    public function cart_products()
    {
        return response()->json(['data' => $this->cart_data()]);
    }

    public function  cart()
    {
        $data = $this->cart_data();
        return view('frontend.pages.cart', compact('data'));
    }

    private function cart_data()
    {
        $productsQuery = Cart::query();
        $data = [];
        $s_product = [];
        $total = 0;

        if (Auth::check()) {
            $productsQuery->where('user_id', Auth::user()->id);
        } elseif (session()->has('temp_user_id')) {
            $temp_user_id = session()->get('temp_user_id');
            $productsQuery->where('temp_user_id', $temp_user_id);
        } else {
            $data['products'] = $s_product;
            $data['total'] = $total;
            return $data;
        }


        $carts = $productsQuery->with([
            'product' => function ($query) {
                $query->select('id', 'name', 'slug', 'hasAttribute', 'image', 'price', 'discount', 'discount_type', 'discount_start', 'discount_end');
                $query->select('id', 'name', 'slug', 'hasAttribute', 'image', 'price', 'discount', 'discount_type', 'discount_start', 'discount_end');
            }
        ])->get();

        foreach ($carts as $cart) {
            $color = '';
            $size = '';
            $total += $cart->sub_total;

            if($cart->colorsize)
            {
                $color = $cart->colorsize->product_color->color->name;
                $size = $cart->colorsize->size->name;
            }

            $s_product[] = [
                'cart_id' => $cart->id,
                'name' => $cart['product']->name,
                'slug' => $cart['product']->slug,
                'color' => $color,
                'size' => $size,
                'quantity' => $cart->quantity,
                'image' => getImageUrl($cart['product']->image),
                'discount' => $cart['discount'],
                'price' => $cart->price,
                'sub_total' => $cart->sub_total,
            ];
        }
        $data['products'] = $s_product;
        $data['total'] = $total;

        return $data;
    }

    public function remove_item(Cart $cart)
    {
        $quantity = $cart->quantity;

        if($cart->color_size != null)
        {
            $color_size = ColorSize::find($cart->color_size);
            $color_size->increment('stock', $quantity);
            $color_size->decrement('number_of_sell', $quantity);
        }

        $product = $cart->product;
        $product->increment('stock', $quantity);
        $product->decrement('number_of_sell', $quantity);
        $cart->delete();

        Toastr::success('Removed! <br> The product has been removed.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);

        return redirect()->back();
    }

    public function remove_all()
    {
        $productsQuery = Cart::query();

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $productsQuery->where('user_id', $user_id);
        } else {
            if (session()->has('temp_user_id')) {
                $temp_user_id = session()->get('temp_user_id');
                $productsQuery->where('temp_user_id', $temp_user_id);
            }
        }

        $carts = $productsQuery->select('id')->get();

        try {
            foreach ($carts as $key => $cart) {
                $cart->delete();
            }

            Toastr::success('Removed! <br> All product has been removed.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();

        } catch (\Exception $e) {
            Toastr::success($e->getMessage(), 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cart_id' => 'required|array|min:1',
            'cart_id.*' => 'numeric',
            'quantity' => 'required|array|min:1',
            'quantity.*' => 'numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $validator = $validator->validated();

        try {
            DB::beginTransaction();
            $totalAmount = 0;
            foreach ($validator['cart_id'] as $key => $cart)
            {
                $cart = Cart::find($cart);
                $quantity = $validator['quantity'][$key];

            // Update the cart quantity
            $cart->update(['quantity' => $quantity]);

            // Update the sub_total based on the new quantity
            $cart->sub_total = ($cart->price - $cart->discount) * $quantity;
            $cart->save();

            // Update the total amount
            $totalAmount += $cart->sub_total;
            }
            $order = Order::find($cart->order_id);
            if ($order) {
                $order->total = $totalAmount;
                $order->save();
            }

            DB::commit();
            Toastr::success('Cart Update Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();

        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::success($e->getMessage(), 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        }
    }

    // Checkout
    public function checkout()
    {
        $data = $this->cart_data();
        return view('frontend.pages.checkout', compact('data'));
    }

    public function cart_checkout(Request $request)
    {
        if(Auth::check())
        {
            $user_id = Auth::user()->id;
        } else {
            return redirect()
                        ->route('login')
                        ->withErrors('Please Login First!');
        }

        $carts = Cart::where('user_id', $user_id)->get();

        if(!$carts)
        {
            Toastr::error('Cart is Empty! <br> Please add Product on Cart.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }

        $order = Validator::make($request->all(), [
            'shipping_charge' => 'nullable|string|max:255'
        ]);

        if ($order->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($order)
                        ->withInput();
        }
        $order = $order->validated();

        $payment = Validator::make($request->all(), [
            'payment_method' => 'required|string|max:255',
            'banking_name' => 'nullable|string|max:255',
            'banking_number' => 'nullable|string|max:255',
            'amount' => 'nullable|numeric',
            'transaction_id' => 'nullable|string|max:255',
        ]);

        if ($payment->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($payment)
                        ->withInput();
        }
        $payment = $payment->validated();

        if(!$request->billing_address_id)
        {
            $billing_adderss = Validator::make($request->all(), [
                'first_name' => 'required|string|max:255',
                'last_name' => 'nullable|string|max:255',
                'email' => 'required|email|max:255',
                'phone' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'appartment' => 'nullable|string|max:255',
                'city' => 'required|string|max:255',
                'post_code' => 'nullable|string|max:255',
                'country' => 'required|string|max:255',
                'state' => 'nullable|string|max:255',
                'note' => 'nullable|string',
            ]);

            if ($billing_adderss->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($billing_adderss)
                            ->withInput();
            }

            $billing_adderss = $billing_adderss->validated();
        }


        DB::beginTransaction();

        try {
            // Order Modal
            if($order['shipping_charge'] > 50)
            {
                $order['shipping_area'] = 'Outside Dhaka';
            } else {
                $order['shipping_area'] = 'Inside Dhaka';
            }

            $order['user_id'] = $user_id;
            $timestamp = time();
            $randomComponent = mt_rand(10000, 99999);
            $orderId = 'O_' . $timestamp . '_' . $randomComponent;

            $order['order_id'] = $orderId;
            $order['coupon'] = 0;
            $order['products_price'] = Cart::where('user_id', $user_id)->sum('sub_total');

            $order['total']  = ($order['products_price'] + $request->shipping_charge) - $order['coupon'];

            $order = Order::create($order);
              //return $carts;
            // OrderedProduct Model
            foreach ($carts as $cart) {
                $color = '';
                $size = '';
                if($cart->colorsize)
                {
                    $color = $cart->colorsize->product_color->color->name;
                    $size = $cart->colorsize->size->name;
                }
                OrderedProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $cart->product_id,
                    'size' => $size,
                    'color' => $color,
                    'quantity' => $cart->quantity,
                    'price' => $cart->price,
                    'discount' => $cart->discount,
                    'sub_total' => $cart->sub_total,
                ]);
                $cart->delete();
            }

            // BillingAddress Modal
            $billing_adderss['user_id'] = $user_id;
            $billing_adderss['order_id'] = $order->id;
            $billing_adderss['billing_address_id'] = $request->billing_address_id ?? null;
            BillingAddress::create($billing_adderss);

            // Payment Model
            $payment['order_id'] = $order->id;
            Payment::create($payment);

            DB::commit();
            Toastr::success('Order Placed Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->route('order-success', $order->order_id);

        } catch (\Exception $e) {
            DB::rollback();
             return redirect()->back()->withInput()->withErrors(['error' => 'Try Again!! <br>'. $e->getMessage()]);
        }
    }

    public function invoice($order)
    {
        $order = Order::with('billing', 'products', 'payment')->where('order_id', $order)->first();

        return view('frontend.pages.invoice', compact('order'));
    }

    public function orderCancel(Request $request)
    {
        $input = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'note' => 'nullable|string',
        ]);

        if ($input->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($input)
                        ->withInput();
        }
        $input = $input->validated();

        $order = Order::find($input['order_id']);

        if (!$order) {
            return redirect()->back()->with('error', 'Order not found.');
        }

        if ($order->status == 1 || $order->status == 3) {
            CancelOrder::create($input);
            Toastr::success('Cancle Request Successfully Placed.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        } else {
            Toastr::success('You can not Cancle. Product Already in '. OrderStatus::from($order->status)->title(), 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        }
        return redirect()->back();
    }

    public function offerCart(Request $request, $slug)
    {
        $color_size_id = null;
        $validator = Validator::make($request->all(), [
            'quantity' => 'nullable|numeric',
            'offer_id' => 'required|exists:offers,id',
            'color_size' => 'nullable|exists:color_sizes,id'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $validator = $validator->validated();

        $product = Product::where('slug', $slug)->first();

        if(!$product)
        {
            Toastr::success('Product Not Found.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }
        if($product->stock < $validator['quantity'])
        {
            Toastr::success('Not Enough Stock.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        }

        $cart = Cart::where('product_id', $product->id);
        $discount = 0;
        $price = $product->price;
        if($request->offer_id)
        {
            $offer = Offer::find($request->offer_id);
            $currentDate = Carbon::now()->format('YmdHis');
            $discountStart = Carbon::parse($offer->offer_start)->format('YmdHis');
            $discountEnd = Carbon::parse($offer->offer_end)->format('YmdHis');

            if ($currentDate >= $discountStart && $currentDate <= $discountEnd) {
                if ($offer->offer_type === 'flat') {
                    $discount = $offer->offer_amount;
                } elseif ($offer->offer_type === 'percent') {
                    $discount = ($product->price * $offer->offer_amount / 100);
                }
            }
        }

        try {
            DB::beginTransaction();

            if($product->hasAttribute == 1)
            {
                if(!isset($validator['color_size']))
                {
                    Toastr::success('Select Color and Size.', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                    return redirect()->back();
                }
                $color_size = ColorSize::find($validator['color_size']);

                $color_size_id = $color_size->id;
                if($color_size->stock < $validator['quantity'])
                {
                    Toastr::success('Not Enough Stock..', 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                    return redirect()->back();
                }
                $color_size->decrement('stock', $validator['quantity']);
                $color_size->increment('number_of_sell', $validator['quantity']);
            }

            $data = [
                'product_id' => $product->id,
                'color_size' => $color_size_id,
                'quantity' => $validator['quantity'],
                'price' => $price,
                'discount' => $discount,
                'sub_total' => ($price - $discount) * $validator['quantity'],
            ];

            if(Auth::check())
            {
                session()->forget('temp_user_id');
                $data['user_id'] = Auth::user()->id;
                $cart->where('user_id', $data['user_id']);
            } else {

                if (!session()->has('temp_user_id')) {
                    $tempUserId = Str::uuid();
                    session()->put('temp_user_id', $tempUserId);
                }

                $data['temp_user_id'] = session('temp_user_id');
                $cart->where('temp_user_id', $data['temp_user_id']);
            }

            $cart = $cart->first();

            if($cart)
            {
                Toastr::success('Product is already in the cart.', 'info', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
                return redirect()->back();
            }

            Cart::create($data);
            $product->decrement('stock', $validator['quantity']);
            $product->increment('number_of_sell', $validator['quantity']);

            DB::commit();
            Toastr::success('Cart Update Successfully.', 'success', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
            return redirect()->back();
        } catch (\Exception $e) {

            DB::rollBack();
            Toastr::success($e->getMessage(), 'error', ["positionClass" => "toast-top-right","closeButton" => true,"progressBar" => true]);
        }
    }
}
