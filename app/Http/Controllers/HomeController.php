<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\SiteSetting;
use App\Models\Product;
use App\Models\Offer;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $data = [];

        // Fetch the categories and their products
        $categoriesWithProducts = Category::whereIn('slug', ['men', 'women', 'kids-babies'])
            ->with([
                'products' => function ($query) {
                    $query->select('id', 'category_id', 'name', 'image', 'thumbnail', 'slug', 'price', 'discount', 'discount_start', 'discount_end', 'discount_type')
                        ->with('images')
                        ->where('stock', '>', 0)
                        ->where('status', 1)
                        ->take(10);
                }
            ])
            ->select('id', 'name', 'slug')
            ->get();

        $data['best_sell'] = $categoriesWithProducts->map(function ($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => $category->slug,
                'products' => $category->products,
            ];
        });

        // Fetch latest products
        $data['latest_products'] = Product::where('status', 1)
                                    ->where('stock', '>', 0)
                                    ->select('id', 'name', 'image', 'thumbnail', 'slug', 'price', 'discount', 'discount_start', 'discount_end', 'discount_type')
                                    ->with('images')
                                    ->latest()
                                    ->take(8)
                                    ->get();
        $data['brands'] = Brand::where('status', 1)
                                    ->select('id', 'name', 'image', 'slug')
                                    ->latest()
                                    ->take(12)
                                    ->get();
        $data['categories'] = Category::where('status', 1)
                                    ->select('id', 'name', 'meta_image', 'slug')
                                    ->inRandomOrder()
                                    ->take(10)
                                    ->get();

        // Fetch offers and organize them into $data
        $offer = SiteSetting::where('title', 'home')->first();

        if (isset($offer->value)) {
            $offer_value = json_decode($offer->value);
            $data['section_1'] = Offer::find($offer_value->section_one);
            $data['section_2'] = Offer::find($offer_value->section_two);
            $data['section_3'] = Offer::find($offer_value->section_three);
        }

        return view('frontend.index', compact('data'));
    }

    public function  login()
    {
        return view('frontend.pages.logIn.index');
    }
    public function about()
    {
        $page_title = 'about us';
        $about = SiteSetting::where('title', 'about')->first();
        $value = json_decode($about->value)->value;

        return view('frontend.pages.customer-service.about', compact('value', 'page_title'));
    }
    public function contact_us()
    {
        $socal_address = SiteSetting::where('title', 'Addresses')->first();
        $value = json_decode($socal_address->value);
        return view('frontend.pages.customer-service.contact', compact('value'));
    }
    public function faq()
    {
        $page_title = "Faq's";
        $faq = SiteSetting::where('title', 'faq')->first();
        $value = json_decode($faq->value)->value;

        return view('frontend.pages.customer-service.faq', compact('value', 'page_title'));
    }
    public function privacy_policy()
    {
        $page_title = 'Privacy Policy';
        $privacy = SiteSetting::where('title', 'privacy')->first();
        $value = json_decode($privacy->value)->value;

        return view('frontend.pages.customer-service.privacy-policy', compact('value', 'page_title'));
    }
    public function terms_condition()
    {
        $page_title = '';
        $terms_condition = SiteSetting::where('title', 'terms_condition')->first();
        $value = json_decode($terms_condition->value)->value;

        return view('frontend.pages.customer-service.terms-condition', compact('value', 'page_title'));
    }
    public function shipping_policy()
    {
        $page_title = 'shipping_policy';
        $shipping_policy = SiteSetting::where('title', 'shipping_policy')->first();
        $value = json_decode($shipping_policy->value)->value;

        return view('frontend.pages.customer-service.shipping-policy', compact('value', 'page_title'));
    }
    public function dashboard()
    {
        $data = [];
        $data['orders'] = Order::where('user_id', Auth::user()->id)->get();

        return view('frontend.pages.dashboard', compact('data'));
    }

}
