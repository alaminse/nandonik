<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Traits\Upload;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    use Upload;

    public function index()
    {
        $categories = Category::orderBy('status')->get();
        return view('backend.category.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.category.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200|unique:categories,name',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:1024'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Retrieve the validated input...
        $validated = $validator->validated();

        if($request->file('meta_image'))
        {
            $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'category', 	1200, 627);
        }
        $validated['slug'] = Str::slug($request->name);

        try {
            Category::create($validated);
            return redirect()->back()->with('success', 'Category Create Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show (Category $category)
    {
        return view('backend.category.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('backend.category.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:200',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:1024',
            'status' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Retrieve the validated input...
        $validated = $validator->validated();

        try {
            if($request->file('meta_image'))
            {
                if($category->meta_image != null)
                {
                    $this->deleteFile($category->meta_image, 'category');
                }
                $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'category', 	1200, 627);
            }
            $validated['slug'] = Str::slug($request->name);


            $category->update($validated);
            return redirect()->back()->with('success', 'Update Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy(Category $category)
    {

        $update = $category->update(['status' => Status::DELETED()]);

        if(!$update) return redirect()->back()->with('error', 'Not DELETED! Try Again!!!');

        return redirect()->back()->with('success', 'Category DELETED Successfully');
    }

    public function status(Category $category)
    {
        $update = $category->update(['status' => $category->status == 1 ? 2 : 1]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }
}
