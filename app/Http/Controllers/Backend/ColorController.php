<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::orderBy('status')->get();
        return view('backend.color', compact('colors'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|unique:colors,name',
            'code'=>'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {
            $input['slug'] = Str::slug($input['name']);
            Color::create($input);

            return redirect()->back()->with('success', 'Color Created Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function update(Request $request, Color $color)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|unique:colors,name,'.$color->id,
            'code'=>'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {
            $input['slug'] = Str::slug($input['name']);
            $color->update($input);

            return redirect()->back()->with('success', 'Color Update Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }
}
