<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\CancelOrder;
use App\Models\Order;
use App\Models\Payment;
use App\Traits\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    use Upload;
    public function index($status)
    {
        $order_status = null;
        $data = ['title' => 'Order Lists'];
        $orders = Order::query();
        switch ($status) {
            case 'all_order':
                break;
            case 'pending':
                $order_status = 1;
                break;
            case 'cancle':
                $order_status = 2;
                break;
            case 'confirm':
                $order_status = 3;
                break;
            case 'ready_to_ship':
                $order_status = 4;
                break;
            case 'out_for_delivery':
                $order_status = 5;
                break;
            case 'delivered':
                $order_status = 6;
                break;
            default:
                abort(404);
                break;
        }

        if ($order_status !== null) {
            $orders = $orders->where('status', $order_status);
        }

        $data['orders'] = $orders->latest()->get();

        return view('backend.order.index', compact('data'));
    }
    protected function getOrdersByStatus($status)
    {
        return Order::where('status', $status)->latest()->get();
    }
    public function show (Order $order)
    {
        $order->load('billing', 'products', 'payment');
        return view('backend.order.show', compact('order'));
    }

    public function updateStatus(Request $request, Order $order)
    {
        $status = $request->input('status');

        $update = $order->update(['status' => $status]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }

    public function cancle_request()
    {
        $cancels = CancelOrder::latest()->get();

        return view('backend.order.cancle', compact('cancels'));
    }

    public function cancle_statusUpdate(Request $request, CancelOrder $cancle_order)
    {
        $validator = Validator::make($request->all(), [
            'admin_note'=>'required|string',
            'status'=>'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();
        $cancle_order->update($input);

        if($request['status'] == 0)
        {
            $cancle_order->order->update(['status' => 2]);
        }

        return redirect()->back()->with('success', 'Update Successfully');
    }

    public function payment_statusUpdate(Request $request, Payment $payment)
    {
        $payment->update(['payment_status' => $request->payment_status]);

        return redirect()->back()->with('success', 'Update Successfully');
    }

}
