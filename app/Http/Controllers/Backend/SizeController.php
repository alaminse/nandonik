<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SizeController extends Controller
{
    public function index()
    {
        $sizes = Size::orderBy('status')->get();
        return view('backend.size', compact('sizes'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|unique:colors,name',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {
            $input['slug'] = Str::slug($input['name']);
            Size::create($input);

            return redirect()->back()->with('success', 'Size Created Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function update(Request $request, Size $size)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|unique:sizes,name,'.$size->id,
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {
            $input['slug'] = Str::slug($input['name']);
            $size->update($input);

            return redirect()->back()->with('success', 'Size Update Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }
}
