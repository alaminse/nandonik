<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SocalMedia;
use App\Models\SiteSetting;
use App\Traits\Upload;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SocalMediaController extends Controller
{
    use Upload;

    public function index()
    {
        $addresses = SiteSetting::where('title','addresses')->first();
        $value = json_decode($addresses->value);

        $logo = SiteSetting::where('title', 'logo')->first();
        $logo_value = json_decode($logo->value);
        return view('backend.socal_media_address.index', compact('addresses','value', 'logo', 'logo_value'));
    }

    public function update(Request $request, $id )
    {
        $site_info = SiteSetting::find($id);

        $requestData = $request->except('_token');

        $site_info->update([
            'value' => json_encode($requestData)
        ]);

        return redirect()->back()->with('success', 'Update Successfully.');
    }

    public function logo_update(Request $request, SiteSetting $logo_update)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'favicon' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        $value = json_decode($logo_update->value);

        $input['image'] = $value->image;
        $input['favicon'] = $value->favicon;

        if($request->file('image'))
        {
            if($value->image)
            {
                $this->deleteFile($value->image, 'logo');
            }
            $filename = 'logo.jpg';
            $upload_path = "uploads/logo";
            $image = Image::make($request->file('image'))->resize(300, 70);
            $logo = public_path($upload_path).'/'.$filename;
            $image->save($logo);

            $input['image'] = 'logo/'.$filename;
        }

        if($request->file('favicon'))
        {
            if($value->favicon)
            {
                $this->deleteFile($value->favicon, 'logo');
            }
            $filename = 'favicon.jpg';
            $upload_path = "uploads/logo";
            $image = Image::make($request->file('favicon'))->resize(32, 32);
            $favicon = public_path($upload_path).'/'.$filename;
            $image->save($favicon);
            $input['favicon'] = 'logo/'.$filename;
        }

        $update = $logo_update->update([
            'value' => json_encode($input)
        ]);

        if(!$update) return redirect()->back()->with('error', 'Not Update. Try Again!!');
        return redirect()->back()->with('success', 'Update Successfully.');

    }
}
