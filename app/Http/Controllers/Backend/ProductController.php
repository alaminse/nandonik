<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\ColorSize;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductImage;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Traits\Upload;
use Carbon\Carbon;

class ProductController extends Controller
{
    use Upload;

    public function index()
    {
        $title = 'All Products';
        $products = Product::with('category', 'sub_category')->orderBy('status')->get();

        return view('backend.product.index', compact('products', 'title'));
    }

    public function create()
    {
        $colors = Color::get();
        $sizes = Size::get();
        $brands = Brand::get();
        $categories = Category::where('status', 1)->get();
        return view('backend.product.create', compact('categories', 'brands', 'colors', 'sizes'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200|unique:products,name',
            'category_id' => 'required|exists:categories,id',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'brand_id' => 'required|exists:brands,id',
            'price' => 'nullable|numeric|min:0',
            'discount' => 'nullable|numeric',
            'discount_type' => 'nullable|string',
            'discount_start' => 'nullable',
            'discount_end' => 'nullable|after:discount_start',
            'quantity' => 'nullable|numeric',
            'hasAttribute' => 'nullable',
            'tags' => 'nullable|string',
            'description' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'thumbnail' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'meta_title' => 'nullable|string',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'gallery' => 'nullable|array|min:1',
            'gallery.*' => 'image|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'quantities' => 'nullable|array',
            'quantities.*' => 'numeric',
            'images' => 'nullable|array|min:1',
            'images.*' => 'image|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $product = $validator->validated();

        DB::beginTransaction();

        try {

            if ($request->discount_start) {
                $start = Carbon::createFromFormat('m/d/Y g:i A', $request->discount_start);
                $product['discount_start'] = $start->format('Y-m-d H:i:s');
            } else {
                $product['discount_start'] = null;
            }

            if ($request->discount_end) {
                $end = Carbon::createFromFormat('m/d/Y g:i A', $request->discount_end);
                $product['discount_end'] = $end->format('Y-m-d H:i:s');
            } else {
                $product['discount_end'] = null;
            }

            $product['slug'] = Str::slug($request->name);

            if($request->file('image'))
            {
                $product['image'] = $this->uploadFile($request->file('image'), 'product', 300, 420);
            }
            if($request->file('thumbnail'))
            {
                $product['thumbnail'] = $this->uploadFile($request->file('thumbnail'), 'product', 300, 420);
            }

            if($request->file('meta_image'))
            {
                $product['meta_image'] = $this->uploadFile($request->file('meta_image'), 'product', 1200, 627);
            }

            if(isset($product['hasAttribute']) == 'on')
            {
                $product['quantity'] = 0;
                foreach ($product['quantities'] as $key => $value) {
                    $product['quantity'] += $value;
                }

                $product['hasAttribute'] = 1;
            }

            $product['stock'] = $product['quantity'];

            $product = Product::create($product);

            if($request->gallery)
            {
                foreach ($request->gallery as $img)
                {
                    $image = null;
                    if($img)
                    {
                        $image = $this->uploadFile($img, 'product', 500, 700);
                    }
                    ProductImage::create([
                        'product_id' => $product->id,
                        'image' => $image
                    ]);
                }
            }

            if(isset($product['hasAttribute']) == 1)
            {
                $i = 0;
                foreach ($request->color_id as $colorId) {
                    $product_color = ProductColor::create([
                        'product_id' => $product->id,
                        'color_id' => $colorId
                    ]);
                    foreach ($request->size_id as $sizeId) {
                        $image = $this->uploadFile($request->images[$i], 'product', 300, 420);
                        ColorSize::create([
                            'product_color_id' => $product_color->id,
                            'size_id' => $sizeId,
                            'quantity' => $request->quantities[$i],
                            'stock' => $request->quantities[$i],
                            'image' => $image
                        ]);

                        $i++;
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('success', 'Product Create Successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show(Product $product)
    {
        $product->load('images', 'colorSize');

        return view('backend.product.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $brands = Brand::get();
        $categories = Category::where('status', 1)->get();
        return view('backend.product.edit', compact('product', 'categories','brands'));
    }

    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200|unique:products,name,'.$product->id,
            'category_id' => 'required|exists:categories,id',
            'brand_id' => 'required|exists:brands,id',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'price' => 'required|numeric|min:0',
            'discount' => 'nullable|numeric',
            'discount_type' => 'nullable|string',
            'discount_start' => 'nullable',
            'discount_end' => 'nullable',
            'tags' => 'nullable|string',
            'hasAttribute' => 'nullable|numeric',
            'description' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'thumbnail' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {


            if ($request->discount_start) {
                $start = Carbon::createFromFormat('m/d/Y g:i A', $request->discount_start);
                $input['discount_start'] = $start->format('Y-m-d H:i:s');
            } else {
                $input['discount_start'] = null;
            }

            if ($request->discount_end) {
                $end = Carbon::createFromFormat('m/d/Y g:i A', $request->discount_end);
                $input['discount_end'] = $end->format('Y-m-d H:i:s');
            } else {
                $input['discount_end'] = null;
            }
            $input['slug'] = Str::slug($request->name);

            if($request->file('image'))
            {
                if($product->image != null)
                {
                    $this->deleteFile($product->image, 'product');
                }
                $input['image'] = $this->uploadFile($request->file('image'), 'product', 300, 420);
            }

            if($request->file('thumbnail'))
            {
                if($product->thumbnail != null)
                {
                    $this->deleteFile($product->thumbnail, 'product');
                }
                $input['thumbnail'] = $this->uploadFile($request->file('thumbnail'), 'product', 300, 420);
            }

            $product->update($input);

            return redirect()->back()->with('success', 'Product Update Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function status(Product $product)
    {
        $update = $product->update(['status' => $product->status == 1 ? 2 : 1]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }

    public function image_update(Request $request, Product $product)
    {
        if (!$product) {
            return response()->json(['error' => 'Product not found'], 404);
        }

        $validator = Validator::make($request->all(), [
            'gallery' => 'required|array|min:1',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();

        try {

            foreach ($request->gallery as $img)
            {
                $image = null;
                if($img)
                {
                    $image = $this->uploadFile($img, 'product', 500, 700);
                }
                ProductImage::create([
                    'product_id' => $product->id,
                    'image' => $image
                ]);
            }

            DB::commit();
            return redirect()->back()->with('success', 'Image Update Successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function image_delete(ProductImage $image)
    {
        if (!$image) {
            return response()->json(['message' => 'Image not found'], 404);
        }
        if($image->image){
            $this->deleteFile($image->image, 'product');
        }
        $image->delete();

        return response()->json(['message' => 'Image Deleted successfully']);
    }

    public function meta_update(Request $request, Product $product)
    {

        if (!$product) {
            return response()->json(['error' => 'Product not found'], 404);
        }

        $validator = Validator::make($request->all(), [
            'meta_title' => 'required|string',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $input = $validator->validated();

        try {
            if($request->file('meta_image'))
            {
                if($product->meta_image != null)
                {
                    $this->deleteFile($product->meta_image, 'product');
                }
                $input['meta_image'] = $this->uploadFile($request->file('meta_image'), 'product', 	1200, 627);
            }
            $product->update($input);

            return redirect()->back()->with('success', 'Update Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function getSubcategories(Category $category)
    {
        if (!$category) {
            return response()->json(['error' => 'Category not found.'], 404);
        }

        $subcategories = $category->sub_categories->filter(function ($subcategory) {
            return $subcategory->status == 1;
        });

        return response()->json($subcategories);
    }
    public function stockOut()
    {
        $title = 'Stock Out Products';
        $products = Product::with('category', 'sub_category')->where('stock', '<=', 2)->orderBy('id', 'DESC')->get();
        return view('backend.product.index', compact('products', 'title'));
    }

    public function color_size(Color $color, Size $size)
    {
        $data =[
            'color' => $color->name,
            'size' => $size->name
        ];
        return response()->json( $data);
    }
    public function varient_update(Request $request, ColorSize $color_size)
    {
        //return $color_size;
        $validator = Validator::make($request->all(), [
            'quantity' => 'nullable|numeric',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',

        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();
        try {

            if ($request->file('image')) {
                if ($color_size->image != null) {
                    $this->deleteFile($color_size->image, 'product');
                }
                $input['image'] = $this->uploadFile($request->file('image'), 'product', 400, 420);
            }

            $color_size->update(['quantity' => $input['quantity'], 'image' => $input['image']]);




            return redirect()->back()->with('success', 'Product Update Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }

    }
}
