<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Order;
use App\Models\Category;
use App\Models\Brand;
use App\Models\User;
use Artisan;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = [];
        $data['users'] = User::count();
        $data['product'] = Product::count();
        $data['orderCount'] = Order::count();
        $data['catagory'] = Category::count();
        $data['brand'] = Brand::count();
        $data['stock'] = Product::sum('stock');

        $data['available_products'] = Product::with('category', 'sub_category')->where('stock', '>', 2)->orderBy('id', 'DESC')->get();
        $data['stock_out_products'] = Product::with('category', 'sub_category')->where('stock', '<=', 2)->orderBy('id', 'DESC')->get();
        $data['latest_order'] = Order::latest()->get();
        return view('backend.index', compact('data'));
    }
    public function allTransactions()
    {
        $transections = Payment::orderBy('id', 'DESC')->get();
        return view('backend.transection', compact('transections'));
    }
    public function customerLists()
    {
        $users = User::doesntHave('roles')->get();
        return view('backend.user_lists', compact('users'));
    }
    public function cacheClear()
    {
        Artisan::call('cache:clear');
        Artisan::call('route:cache');
        Artisan::call('config:cache');
        Artisan::call('view:clear');

        return redirect()->back()->with('success', 'Offer Update Successfully');
        // return redirect()->back()->with('success', 'Cache Clear Successfully');
    }
}
