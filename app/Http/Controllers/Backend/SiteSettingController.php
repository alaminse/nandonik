<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteSetting;
use App\Models\Offer;
use App\Models\SendMessage;

class SiteSettingController extends Controller
{
    public function home()
    {
        $data['service'] = SiteSetting::where('title','home')->first();
        $data['value'] = json_decode($data['service']->value);
        $data['offers'] = Offer::where('status', 1)->get();
        return view('backend.pages.home', compact('data'));
    }

    public function about(){
        $service=SiteSetting::where('title','about')->first();
        $value= json_decode($service->value)->value;
        return view('backend.pages.about',compact('service','value'));
    }
    public function privacy(){
        $service=SiteSetting::where('title','privacy')->first();
        $value= json_decode($service->value)->value;
        return view('backend.pages.privacy-policy',compact('service','value'));
    }
    public function faq(){
        $service=SiteSetting::where('title','faq')->first();
        $value= json_decode($service->value)->value;
        return view('backend.pages.faq',compact('service','value'));
    }
    public function terms(){
        $service=SiteSetting::where('title','terms_condition')->first();
        $value= json_decode($service->value)->value;
        return view('backend.pages.terms-condition',compact('service','value'));
    }

    public function shipping()
    {
        $service = SiteSetting::where('title','shipping_policy')->first();
        $value= json_decode($service->value);
        return view('backend.pages.shipping-policy',compact('service','value'));
    }

    public function contact(){
        $service= SendMessage::orderBy('name','asc')->get();
        return view('backend.pages.contact',compact('service'));
    }
    public function update(Request $request, SiteSetting $service)
    {
        $requestData = $request->except('_token');
        $service->update([
            'value' => json_encode($requestData)
        ]);

        return redirect()->back()->with('success', 'Update Successfully.');
    }
}
