<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use App\Traits\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SubCategoryController extends Controller
{
    use Upload;

    public function index()
    {
        $sub_categories = SubCategory::orderBy('status')->get();

        return view('backend.sub_category.index', compact('sub_categories'));
    }

    public function create()
    {
        $categories = Category::where('status', Status::ACTIVE())->get();
        return view('backend.sub_category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200|unique:sub_categories,name',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'category_ids' => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $validated = $validator->validated();

        try {

            if($request->file('meta_image'))
            {
                $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'sub_category', 	1200, 627);
            }

            $validated['slug'] = Str::slug($request->name);

            $sub_category = SubCategory::create($validated);
            $sub_category->categories()->attach($request->category_ids);

            return redirect()->back()->with('success', 'Sub Category Create Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show (SubCategory $sub_category)
    {
        return view('backend.sub_category.show', compact('sub_category'));
    }

    public function edit(SubCategory $sub_category)
    {

        $categories = Category::where('status', Status::ACTIVE())->get();
        return view('backend.sub_category.edit', compact('sub_category','categories'));
    }

    public function update(Request $request, SubCategory $sub_category)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:200',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'status' => 'required|numeric',
            'category_ids' => 'nullable',
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Retrieve the validated input...
        $validated = $validator->validated();

        try {
            if($request->file('meta_image'))
            {
                if($sub_category->meta_image != null)
                {
                    $this->deleteFile($sub_category->meta_image, 'sub_category');
                }
                $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'sub_category', 	1200, 627);
            }
            $validated['slug'] = Str::slug($request->name);


            $sub_category->update($validated);
            $sub_category->categories()->sync($request->category_ids);
            return redirect()->back()->with('success', 'Update Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy(SubCategory $sub_category)
    {

        $update = $sub_category->update(['status' => Status::DELETED()]);

        if(!$update) return redirect()->back()->with('error', 'Not DELETED! Try Again!!!');

        return redirect()->back()->with('success', 'Sub Category DELETED Successfully');
    }

    public function status(SubCategory $sub_category)
    {
        $update = $sub_category->update(['status' => $sub_category->status == 1 ? 2 : 1]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }
}
