<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailTest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Str;
use App\Traits\Upload;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class OfferController extends Controller
{
    use Upload;

    public function index()
    {
        $offers = Offer::orderBy('status')->get();
        return view('backend.offer.index',compact('offers'));
    }

    public function create()
    {
        return view('backend.offer.create');
    }

    public function store(Request $request)
    {

        // 'product_ids' => [
        //     'nullable',
        //     'array',
        //     Rule::exists('products', 'id'),
        // ],
        $validator = Validator::make($request->all(), [
            'title'=>'required|string|unique:offers,title',
            'description'=>'nullable|string',
            'offer_start' => 'required|date',
            'offer_end' => 'required|date|after:offer_start',
            'button_name'=>'nullable|string',
            'button_url'=>'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'offer_type'=>'required|string',
            'offer_amount'=>'required|numeric|min:0',
            'tags'=>'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {

            if ($request->offer_start) {
                $start = Carbon::createFromFormat('m/d/Y g:i A', $request->offer_start);
                $input['offer_start'] = $start->format('Y-m-d H:i:s');
            } else {
                $input['offer_start'] = null;
            }

            if ($request->offer_end) {
                $end = Carbon::createFromFormat('m/d/Y g:i A', $request->offer_end);
                $input['offer_end'] = $end->format('Y-m-d H:i:s');
            } else {
                $input['offer_end'] = null;
            }

            if($request->file('image'))
            {
                $input['image'] = $this->uploadFile($request->file('image'), 'offer', 1779, 900);
            }

            $input['slug'] = Str::slug($request->title);
            Offer::create($input);

            return redirect()->back()->with('success', 'Offer Created Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show(Offer $offer)
    {
        return view('backend.offer.show', compact('offer'));
    }

    public function edit(Offer $offer)
    {
        $products = Product::where('status', 1)->get();
        return view('backend.offer.edit', compact('offer', 'products'));
    }

    public function update(Request $request, Offer $offer)
    {
        $validator = Validator::make($request->all(), [
            'title'=>'required|string|unique:offers,title,'.$offer->id,
            'description'=>'nullable|string',
            'offer_start' => 'required|date',
            'offer_end' => 'required|date|after:offer_start',
            'button_name'=>'nullable|string',
            'button_url'=>'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'status' => 'required|numeric',
            'offer_type'=>'required|string',
            'offer_amount'=>'required|numeric|min:0',
            'tags'=>'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $validator->validated();

        try {
            if ($request->offer_start) {
                $start = Carbon::createFromFormat('m/d/Y g:i A', $request->offer_start);
                $input['offer_start'] = $start->format('Y-m-d H:i:s');
            } else {
                $input['offer_start'] = null;
            }

            if ($request->offer_end) {
                $end = Carbon::createFromFormat('m/d/Y g:i A', $request->offer_end);
                $input['offer_end'] = $end->format('Y-m-d H:i:s');
            } else {
                $input['offer_end'] = null;
            }
            $input['product_ids'] = json_encode($request->product_ids);

            if($request->file('image'))
            {
                if($offer->image != null)
                {
                    $this->deleteFile($offer->image, 'offer');
                }
                $input['image'] = $this->uploadFile($request->file('image'), 'offer', 1779, 900);
            }

            $input['slug'] = Str::slug($request->title);

            $offer->update($input);

            return redirect()->back()->with('success', 'Offer Update Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function status(Offer $offer)
    {
        $update = $offer->update(['status' => $offer->status == 1 ? 2 : 1]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }

    public function destroy(Offer $offer)
    {
        $update = $offer->delete();
        if(!$update) return redirect()->back()->with('error', 'Not DELETED! Try Again!!!');
        return redirect()->back()->with('success', 'Category DELETED Successfully');
    }

    public function send_sms(Offer $offer)
    {
        return view('backend.offer.sendSMS', compact('offer'));
    }

    public function send_mail(Request $request)
    {
        $details['subject'] = $request->subject;
        $details['description'] = $request->description;

        $users = User::select('email')->get();
        foreach ($users as $user)
        {
            $details['email'] = $user;
            dispatch(new SendEmailTest($details));
        }
        return redirect()->back()->with('success', 'Successfull');
    }

}
