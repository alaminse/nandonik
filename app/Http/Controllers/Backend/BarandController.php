<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Traits\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Enums\Status;

class BarandController extends Controller
{
    use Upload;

    public function index()
    {
        $brands = Brand::orderBy('status')->get();

        return view('backend.brand.index', compact('brands'));
    }
    public function create()
    {
        return view('backend.brand.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:200|unique:brands,name',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $validated = $validator->validated();

        try {

            if($request->file('image'))
            {
                $validated['image'] = $this->uploadFile($request->file('image'), 'brand', 300, 300);
            }

            if($request->file('meta_image'))
            {
                $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'brand', 1200, 627);
            }

            $validated['slug'] = Str::slug($request->name);

            Brand::create($validated);

            return redirect()->back()->with('success', 'Brand Create Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show (Brand $brand)
    {
        return view('backend.brand.show', compact('brand'));
    }

    public function edit(Brand $brand)
    {

        $brands = Brand::where('status', Status::ACTIVE())->get();
        return view('backend.brand.edit', compact('brand','brands'));
    }

    public function update(Request $request, Brand $brand)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:200',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,webp|max:1024',
            'meta_title' => 'nullable|string|max:2000',
            'meta_description' => 'nullable|string',
            'meta_image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:1024',
            'status' => 'required|numeric',

        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Retrieve the validated input...
        $validated = $validator->validated();

        try {

            if($request->file('image'))
            {
                if($brand->image != null)
                {
                    $this->deleteFile($brand->image, 'brand');
                }
                $validated['image'] = $this->uploadFile($request->file('image'), 'brand', 300, 300);
            }

            if($request->file('meta_image'))
            {
                if($brand->meta_image != null)
                {
                    $this->deleteFile($brand->meta_image, 'brand');
                }
                $validated['meta_image'] = $this->uploadFile($request->file('meta_image'), 'brand', 	1200, 627);
            }
            $validated['slug'] = Str::slug($request->name);

            $brand->update($validated);
            return redirect()->back()->with('success', 'Update Successfully');

        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy(Brand $brand)
    {

        $update = $brand->update(['status' => Status::DELETED()]);

        if(!$update) return redirect()->back()->with('error', 'Not DELETED! Try Again!!!');

        return redirect()->back()->with('success', 'Brand DELETED Successfully');
    }

    public function status(Brand $brand)
    {
        $update = $brand->update(['status' => $brand->status == 1 ? 2 : 1]);

        if(!$update) return redirect()->back()->with('error', 'Not Update! Try Again!!!');

        return redirect()->back()->with('success', 'Status Update Successfully');
    }


}
