<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
            ['name' => 'Red', 'slug' => 'red', 'code' => '#FF0000'],
            ['name' => 'Green', 'slug' => 'green', 'code' => '#00FF00'],
            ['name' => 'Blue', 'slug' => 'blue', 'code' => '#0000FF'],
            ['name' => 'Yellow', 'slug' => 'yellow', 'code' => '#FFFF00'],
            ['name' => 'Purple', 'slug' => 'purple', 'code' => '#800080'],
            ['name' => 'Orange', 'slug' => 'orange', 'code' => '#FFA500'],
            ['name' => 'Pink', 'slug' => 'pink', 'code' => '#FFC0CB'],
            ['name' => 'Brown', 'slug' => 'brown', 'code' => '#A52A2A'],
            ['name' => 'Cyan', 'slug' => 'cyan', 'code' => '#00FFFF'],
            ['name' => 'Magenta', 'slug' => 'magenta', 'code' => '#FF00FF'],
            ['name' => 'Teal', 'slug' => 'teal', 'code' => '#008080'],
            ['name' => 'Lime', 'slug' => 'lime', 'code' => '#00FF00'],
            ['name' => 'Maroon', 'slug' => 'maroon', 'code' => '#800000'],
            ['name' => 'Navy', 'slug' => 'navy', 'code' => '#000080'],
            ['name' => 'Olive', 'slug' => 'olive', 'code' => '#808000'],
            ['name' => 'Silver', 'slug' => 'silver', 'code' => '#C0C0C0'],
            ['name' => 'Gray', 'slug' => 'gray', 'code' => '#808080'],
            ['name' => 'Black', 'slug' => 'black', 'code' => '#000000']
        ];

        foreach ($colors as $color) {
            DB::table('colors')->insert($color);
        }

    }
}
