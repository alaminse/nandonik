<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Men', 'Women', 'Kids & Babies'];

        foreach ($categories as $key => $category) {
            Category::create([
                'name' => $category,
                'slug' => Str::slug($category)
            ]);
        }

        for ($i=0; $i < 10; $i++) {
            Category::create([
                'name' => 'Category '. $i,
                'slug' => Str::slug('Category '. $i)
            ]);
        }

        for ($j = 0; $j < 10; $j++)
        {
            $ids = Category::inRandomOrder()->limit(3)->pluck('id')->toArray();

            $sub_category = SubCategory::create([
                'name' => 'Sub Category '.$j,
                'slug' => Str::slug('Sub Category '.$j)
            ]);

            $sub_category->categories()->attach($ids);
        }
    }
}
