<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            'Viable', 'Play', 'Ultra', 'Tway', 'Tracka', 'Tigerie', 'The Hookup Apparel', 'Sweaty Betty', 'Sweat and Strut', 'Strong Muscle Shirts', 'State I am', 'Hydrocean', 'Knigh tingale', 'Land Now', 'Pentagon', 'Shapeme Fitness', 'Shapetastics', 'Shot clock Clothing', 'Sports Street', 'FitFlex', 'ActivFit', 'PowerFlex', 'Yellow', 'EnergizeSport', 'MotionGear', 'Active Plus', 'Annie’s Athleisure', 'Brown berry', 'Champ Camp', 'Sportswear', 'Champions', 'Copper bust', 'Dream Garb', 'Eleven ++', 'Esporto Maniac', 'Every Wears', 'Exerciso', 'Fab & Fit Activewear', 'Gironetic', 'Good American', 'Gymwears', 'Habiliments'
        ];

        foreach ($brands as $brand) {
            Brand::create([
                'name' => $brand,
                'slug' => Str::slug($brand)
            ]);
        }
    }
}
