<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->datetime('offer_start')->nullable();
            $table->datetime('offer_end')->nullable();
            $table->string('button_name')->nullable();
            $table->string('button_url')->nullable();
            $table->text('image')->nullable();
            $table->string('offer_type')->nullable();
            $table->float('offer_amount', 8, 2)->nullable();
            $table->string('tags')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
};
