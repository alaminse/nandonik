<?php

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Size;
use App\Models\SubCategory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->nullable()->constrained((new Category())->getTable());
            $table->foreignId('sub_category_id')->nullable()->constrained((new SubCategory())->getTable());
            $table->foreignId('brand_id')->nullable()->constrained((new Brand())->getTable());
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->text('tags')->nullable();
            $table->boolean('hasAttribute')->default(false);
            $table->float('price', 8, 2);
            $table->float('discount', 8, 2)->nullable();
            $table->string('discount_type')->nullable();
            $table->datetime('discount_start')->nullable();
            $table->datetime('discount_end')->nullable();
            $table->integer('quantity');
            $table->integer('number_of_sell')->default(0);
            $table->integer('stock')->default(0);
            $table->string('meta_title')->nullable();
            $table->string('meta_image')->nullable();
            $table->text('meta_description')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
