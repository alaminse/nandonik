<?php

use App\Models\ColorSize;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained((new Product())->getTable());
            $table->foreignId('order_id')->constrained((new Order())->getTable());
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->tinyInteger('quantity')->default(1);
            $table->double('price')->default(0);
            $table->double('discount')->default(0);
            $table->double('sub_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_products');
    }
};
