<?php

use App\Models\ColorSize;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained((new User())->getTable());
            $table->string('temp_user_id')->nullable();
            $table->foreignId('product_id')->constrained((new Product())->getTable());
            $table->foreignId('color_size')->nullable()->constrained((new ColorSize())->getTable());
            $table->tinyInteger('quantity')->default(1);
            $table->double('price')->default(0);
            $table->double('discount')->default(0);
            $table->double('sub_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
};
